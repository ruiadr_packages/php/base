## Classe "Type"

La classe fournit des méthodes statiques permettant de travailler sur le type des valeurs passées en paramètres.

La méthode **cast()** réalise un traitement explicite en fonction du type de retour souhaité et passé en paramètre. Lorsque le transtypage n'est pas possible, la méthode retourne "null".

```php
Type::cast('1', TypeInterface::INT);     // Retourne int(1)
Type::cast('1', TypeInterface::FLOAT);   // Retourne float(1)
Type::cast('1', TypeInterface::STRING);  // Retourne string("1")
Type::cast([], TypeInterface::INT);      // Retourne NULL
```

La méthode **autoCast()** tente de transtyper une valeur dans le type le plus approprié possible. Si elle n'y parvient pas, la valeur passée en paramètre est retournée telle quelle.

```php
Type::autoCast('1');     // Retourne int(1)
Type::autoCast('1.1');   // Retourne float(1.1)
Type::autoCast('true');  // Retourne bool(true)
Type::autoCast([]);      // Retourne le tableau passé en paramètre
```