## Classe "Parameters"

Apporte une approche objet sur un tableau associatif utilisé pour l'instanciation d'un objet de ce type.

Nous prenons le tableau ci-dessous comme exemple pour la suite de la documentation.

```php
$array = [
    'a' => 1,
    'b' => 'test-b',
    'c' => [
        'c1' => 3,
        'c2' => 'test-c',
        'c3' => '3',
    ],
    'd' => '4',
];
```

### Exemples simples:

```php
$params = new Parameters($array);

$params->get('a');         // Retourne int(1)
$params->getInt('a');      // Retourne int(1)
$params->getFloat('a');    // Retourne float(1)
$params->getString('a');   // Retourne string("1")

$params->get('b');         // Retourne string("test-b")
$params->getInt('b');      // Retourne NULL, 'b' contient une chaîne

// Valeur inexistante. Comportement identique avec les autres getters.
$params->get('x'); // Retourne NULL
```

Un typage fort est automatiquement réalisé en fonction de la valeur associée à une clé.

Par exemple:

```php
$params = new Parameters($array);
$params->get('d'); // Retourne int(4) et non string("4")
```

Il est possible de désactiver ce cast automatique directement depuis le constructeur, ou à l'aide de la méthode **setAutoCast()**.

Par exemple:

```php
$params = new Parameters($array, false);
$params->get('d'); // Retourne string("4");

$params = new Parameters($array);
$params->setAutoCast(false);
$params->get('d'); // Retourne string("4");
```

### Exemples avec une valeur par défaut:

```php
$params = new Parameters($array);

$params->get('a', 2);    // Retourne int(1), 'a' existe et vaut int(1)
$params->getInt('a', 2); // idem que précédemment

// La clé x n'existe pas.
$params->get('x', 1);       // Retourne int(1)
$params->getInt('x', 2);    // Retourne int(2)
$params->getFloat('x', 3);  // Retourne float(3)
$params->getString('x', 4); // Retourne string("4")
```

### Exemples avec une autre approche:

En plus des getters **getInt()**, **getFloat()** et **getString()**, il est possible d'utiliser directement la méthode **get()** en spécifiant un type connu.

Les constantes de types sont disponibles dans `Ruiadr\Base\Common\Interface\TypeInterface` à l'aide de:

- **INT** pour les entiers
- **FLOAT** pour les flottants
- **STRING** pour les chaînes

```php
$params = new Parameters($array);

$params->get('a', null, TypeInterface::INT);    // Retourne int(1)
$params->get('a', null, TypeInterface::FLOAT);  // Retourne float(1)
$params->get('a', null, TypeInterface::STRING); // Retourne string("1")

// La clé x n'existe pas.
$params->get('x', null, TypeInterface::INT); // Retourne NULL
$params->get('x', 1, TypeInterface::INT);    // Retourne int(1)
$params->get('x', 1, TypeInterface::FLOAT);  // Retourne float(1)
$params->get('x', 1, TypeInterface::STRING); // Retourne string("1")
```

### Chaînage

```php
$params = new Parameters($array);

$params->get('c')->get('c1');       // Retourne int(3)
$params->get('c')->get('c3');       // Retourne int(3)
$params->get('c')->getInt('c3');    // Retourne int(3)
$params->get('c')->getFloat('c3');  // Retourne float(3)
$params->get('c')->getString('c3'); // Retourne string("3")
```

### Via les propriétés

Pour simplifier l'utilisation de l'objet, il est possible de faire appel aux clés comme des propriétés publiques (en lecture seule) de l'objet.

```php
$params = new Parameters($array);

$params->a;     // Retourne int(1)
$params->c;     // Retourne un objet "Parameters"
$params->c->c1; // Retourne int(3)
$params->c->c3; // Retourne int(3)
$params->x;     // Retourne NULL
```

Par défaut un cast automatique est réalisé, mais pour rappel, il est possible de le désactiver. En ce sens, nous aurons les résultats suivants:

```php
$params = new Parameters($array, false);
// paramètre false dans le constructeur ou à l'aide de la méthode setAutoCast().

$params->a;     // Retourne int(1)
$params->c;     // Retourne un objet "Parameters"
$params->c->c1; // Retourne int(3)
$params->c->c3; // Retourne string("3") cette fois-ci
$params->x;     // Retourne NULL
```

### Tester la présence d'une clé

La méthode **contains()** permet de vérifier la présence d'une clé dans l'objet.

```php
$params = new Parameters($array);

$params->contains('a'); // Retourne bool(true)
$params->contains('x'); // Retourne bool(false)
```

De la même manière, il devient possible de tester la présence d'une clé, directement en utilisant les propriétés de l'objet, par exemple:

```php
$params = new Parameters($array);

isset($params->a); // Retourne bool(true)
isset($params->b); // Retourne bool(false)
```

### Variables d'environnement

Il est possible de lier une clé à une variable d'environnement. Pour cela, la valeur d'une clé doit être construite de la sorte: `ENV:<LA_VARIABLE_SOUHAITEE>`, exemple:

```php
$array = ['test' => 'ENV:SHELL'];
$params = new Parameters($array);

$params->test; // Retourne string("/bin/bash")
```
