<?php

namespace Ruiadr\Base\Tests\Wrapper;

use PHPUnit\Framework\TestCase;
use Ruiadr\Base\Tests\HtmlParser;
use Ruiadr\Base\Tests\ImageParser;
use Ruiadr\Base\Tests\JavascriptParser;
use Ruiadr\Base\Tests\MyHtmlParser;
use Ruiadr\Base\Tests\MyImageParser;
use Ruiadr\Base\Tests\MyJavascriptParser;
use Ruiadr\Base\Wrapper\Reflection;

final class ReflectionTest extends TestCase
{
    private const BASE_NS = 'Ruiadr\Base\Tests\\';

    private function build(string $type): ?\ReflectionClass
    {
        return Reflection::build($type, 'my', 'parser', self::BASE_NS);
    }

    public function testBuild(): void
    {
        $this->assertInstanceOf(\ReflectionClass::class, $this->build('javascript'));
        $this->assertInstanceOf(\ReflectionClass::class, $this->build('html'));
        $this->assertInstanceOf(\ReflectionClass::class, $this->build('image'));
        $this->assertInstanceOf(\ReflectionClass::class, Reflection::build('Exception'));

        // Pour s'assurer que les tests couvrent bien tout le code.

        $this->assertInstanceOf(\ReflectionClass::class, Reflection::build('Bar'));
        $this->assertInstanceOf(\ReflectionClass::class, Reflection::build('Bar', null, 'Fetcher', 'Foo\\'));
    }

    public function testInstanciate(): void
    {
        // MyJavascriptParser

        $r = $this->build('javascript');
        $this->assertInstanceOf(MyJavascriptParser::class, $r->newInstance());

        // MyHtmlParser

        $r = $this->build('html');
        $this->assertInstanceOf(MyHtmlParser::class, $r->newInstance());

        // MyImageParser

        $r = $this->build('image');
        $this->assertInstanceOf(MyImageParser::class, $r->newInstance());
    }

    public function testBuildPrefix(): void
    {
        // JavascriptParser

        $r = Reflection::buildPrefix('parser', 'javascript', self::BASE_NS);
        $this->assertInstanceOf(JavascriptParser::class, $r->newInstance());

        // HtmlParser

        $r = Reflection::buildPrefix('parser', 'html', self::BASE_NS);
        $this->assertInstanceOf(HtmlParser::class, $r->newInstance());

        // ImageParser

        $r = Reflection::buildPrefix('parser', 'image', self::BASE_NS);
        $this->assertInstanceOf(ImageParser::class, $r->newInstance());
    }

    public function testBuildSuffix(): void
    {
        // JavascriptParser

        $r = Reflection::buildSuffix('javascript', 'parser', self::BASE_NS);
        $this->assertInstanceOf(JavascriptParser::class, $r->newInstance());

        // HtmlParser

        $r = Reflection::buildSuffix('html', 'parser', self::BASE_NS);
        $this->assertInstanceOf(HtmlParser::class, $r->newInstance());

        // ImageParser

        $r = Reflection::buildSuffix('image', 'parser', self::BASE_NS);
        $this->assertInstanceOf(ImageParser::class, $r->newInstance());
    }
}
