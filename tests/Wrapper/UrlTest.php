<?php

namespace Ruiadr\Base\Tests\Wrapper;

use PHPUnit\Framework\TestCase;
use Ruiadr\Base\Wrapper\Interface\UrlInterface;
use Ruiadr\Base\Wrapper\Url;

final class UrlTest extends TestCase
{
    private const DOM_GOOGLE_COM = 'google.com';
    private const DOM_GOOGLE_CO_UK = 'google.co.uk';

    private const SITE_COM = 'monsite.'.self::DOM_GOOGLE_COM;
    private const SITE_CO_UK = 'monsite.'.self::DOM_GOOGLE_CO_UK;
    private const SITE_GOOGLE = 'monsite.google';

    private function buildUrlString(string $domain, string $params = ''): string
    {
        return 'https://www.'.$domain.$params;
    }

    private function buildUrl(string $domain, string $params = ''): UrlInterface
    {
        return new Url($this->buildUrlString($domain, $params));
    }

    private function assertAllProperties(string $domain): void
    {
        $url = $this->buildUrl($domain, '/test.hmtl?p=1');

        $this->assertSame('https', $url->scheme);
        $this->assertSame('www.'.$domain, $url->host);
        $this->assertSame('/test.hmtl', $url->path);
        $this->assertSame('p=1', $url->query);
        $this->assertSame($domain, $url->domain);
    }

    public function testPropertiesCom(): void
    {
        $this->assertAllProperties(self::DOM_GOOGLE_COM);
    }

    public function testPropertiesCoUk(): void
    {
        $this->assertAllProperties(self::DOM_GOOGLE_CO_UK);
    }

    public function testDomains(): void
    {
        $url = $this->buildUrlString(self::SITE_COM);
        $this->assertSame(self::DOM_GOOGLE_COM, (new Url($url))->domain);

        $url = $this->buildUrlString(self::SITE_CO_UK);
        $this->assertSame(self::DOM_GOOGLE_CO_UK, (new Url($url))->domain);

        $url = $this->buildUrlString(self::SITE_GOOGLE);
        $this->assertSame(self::SITE_GOOGLE, (new Url($url))->domain);
    }

    public function testGetUrl(): void
    {
        $this->assertSame(
            $this->buildUrlString(self::DOM_GOOGLE_COM),
            $this->buildUrl(self::DOM_GOOGLE_COM)->getUrl()
        );

        $this->assertSame(
            $this->buildUrlString(self::SITE_GOOGLE),
            $this->buildUrl(self::SITE_GOOGLE)->getUrl()
        );
    }

    public function testGetDomain(): void
    {
        $this->assertIsString($this->buildUrl(self::SITE_COM)->getDomain());
        $this->assertIsString($this->buildUrl(self::SITE_CO_UK)->getDomain());
        $this->assertIsString($this->buildUrl(self::SITE_GOOGLE)->getDomain());
        $this->assertNull((new Url(self::DOM_GOOGLE_COM))->getDomain());
        $this->assertNull((new Url(self::DOM_GOOGLE_CO_UK))->getDomain());
        $this->assertNull((new Url('not_valid'))->getDomain());
    }

    public function testToString(): void
    {
        $urlString = $this->buildUrlString(self::SITE_COM);
        $url = new Url($urlString);
        $this->assertSame("$url", $urlString);

        $url = new Url(self::DOM_GOOGLE_COM);
        $this->assertSame("$url", self::DOM_GOOGLE_COM);
    }
}
