<?php

namespace Ruiadr\Base\Tests\Wrapper;

use PHPUnit\Framework\TestCase;
use Ruiadr\Base\Wrapper\CUrl;
use Ruiadr\Base\Wrapper\Interface\CUrlInterface;

final class CUrlTest extends TestCase
{
    private const URL = 'https://www.google.com/';

    public function testGetUrl(): void
    {
        $curl = new CUrl(self::URL);

        $this->assertSame(self::URL, $curl->getUrl());
    }

    public function testGetOptions(): void
    {
        $options = [
            CURLOPT_TIMEOUT => 100,
            CURLOPT_SSL_VERIFYPEER => true,
        ];

        $curl = new CUrl(self::URL, $options);

        $this->assertSame($options, $curl->getOptions());
    }

    public function testOverrideOptions(): void
    {
        $options = [
            CURLOPT_HEADER => true,
            CURLOPT_NOBODY => true,
        ];

        $curl = new CUrl(self::URL, $options);

        $this->assertNull($curl->getResponse());
        $this->assertSame(200, $curl->getResponseCode());
        $this->assertNotSame('200', $curl->getResponseCode());

        $curl->exec();

        $this->assertIsString($curl->getResponse());
        $this->assertSame(200, $curl->getResponseCode());
        $this->assertNotSame('200', $curl->getResponseCode());
    }

    public function testExec(): void
    {
        $curl = new CUrl(self::URL);

        $this->assertInstanceOf(CUrlInterface::class, $curl->exec());
    }

    public function testGetResponse(): void
    {
        $curl = new CUrl(self::URL);
        $curl->exec();

        $this->assertIsString($curl->getResponse());
        $this->assertSame(200, $curl->getResponseCode());
        $this->assertNotSame('200', $curl->getResponseCode());
    }

    public function testGetInfo(): void
    {
        $curl = new CUrl(self::URL);

        // La méthode exec doit être lancée en interne, donc getInfo() doit
        // pouvoir retourner des résultats valides.
        $this->assertNull($curl->getInfo('helloworld'));
        $this->assertSame(200, $curl->getInfo(CURLINFO_HTTP_CODE));
        $this->assertSame(200, $curl->getInfo('http_code'));
        $this->assertNotSame('200', $curl->getInfo(CURLINFO_HTTP_CODE));
        $this->assertNotSame('200', $curl->getInfo('http_code'));

        $curl = new CUrl(self::URL);
        $curl->exec();

        $this->assertSame(200, $curl->getInfo(CURLINFO_HTTP_CODE));
        $this->assertSame(200, $curl->getInfo('http_code'));
        $this->assertNotSame('200', $curl->getInfo(CURLINFO_HTTP_CODE));
        $this->assertNotSame('200', $curl->getInfo('http_code'));
    }

    public function testContentType(): void
    {
        $curl = new CUrl(self::URL);
        $curl->exec();

        $this->assertSame('text/html', $curl->getContentType());
        $this->assertTrue($curl->isContentType('text/html'));
        $this->assertTrue($curl->isContentType(' Text/HTML  '));
    }

    public function testGetInfos(): void
    {
        $curl = new CUrl(self::URL);

        $infos = $curl->getInfos();
        $this->assertIsArray($infos);
        $this->assertSame([], $infos);

        $this->assertArrayNotHasKey('url', $infos);
        $this->assertArrayNotHasKey('http_code', $infos);

        $curl->exec();

        $infos = $curl->getInfos();
        $this->assertIsArray($infos);
        $this->assertNotSame([], $infos);

        $this->assertArrayHasKey('url', $infos);
        $this->assertSame(self::URL, $infos['url']);

        $this->assertArrayHasKey('http_code', $infos);
        $this->assertSame(200, $infos['http_code']);
    }

    public function testFetch(): void
    {
        $curl = new CUrl(self::URL);
        $this->assertIsString($curl->fetch());

        $curl = new CUrl('hello world');
        $this->assertFalse($curl->fetch());
    }
}
