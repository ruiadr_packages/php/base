<?php

namespace Ruiadr\Base\Tests\File;

use PHPUnit\Framework\TestCase;
use Ruiadr\Base\File\Exception\FileAddException;
use Ruiadr\Base\File\Exception\FileNotFoundException;
use Ruiadr\Base\File\File;
use Ruiadr\Base\File\Interface\DirectoryInterface;
use Ruiadr\Base\File\Interface\FileInterface;
use Ruiadr\Utils\SecurityUtils;

final class FileTest extends TestCase
{
    private static ?FileInterface $file = null;

    public static function getDirectoryPathBase(): string
    {
        return dirname(__FILE__);
    }

    public static function generatedFilePath(): string
    {
        return self::getDirectoryPathBase()
            .'/test_'.SecurityUtils::randomAlphanum().'.txt';
    }

    public function instanciateTestFile(): FileInterface
    {
        if (self::$file instanceof FileInterface) {
            self::$file->remove();
        }
        self::$file = File::create(self::generatedFilePath());

        return self::$file;
    }

    public function testCreateFile(): void
    {
        $path = self::generatedFilePath();

        $file = null;

        try {
            $file = File::create($path);
        } catch (\Exception $e) {
            // Ne rien faire pour permettre à la suite du script
            // de tester l'existence du fichier.
        }

        $this->assertFileExists($path);

        $this->assertSame("$file", $path); // __toString().

        if ($file instanceof FileInterface) {
            $file->remove();
        }
    }

    public function testCreateError(): void
    {
        $this->expectException(FileAddException::class);
        File::create('/not_allowed');
    }

    public function testCreateTreeError(): void
    {
        $this->expectException(FileAddException::class);
        File::createTree('/var/not_allowed');
    }

    public function testBuildError(): void
    {
        $this->expectException(FileNotFoundException::class);
        new File('/not_file');
    }

    public function testWriteFileAppend(): void
    {
        $file = $this->instanciateTestFile();

        // Avec append.

        $this->assertTrue($file->write('test'));
        $data = $file->readAsArray();

        $this->assertIsArray($data);
        $this->assertEquals(1, count($data));
        $this->assertSame('test', $data[0]);

        // Avec append + saut de ligne.

        $this->assertTrue($file->write("\ntest"));
        $data = $file->readAsArray();

        $this->assertIsArray($data);
        $this->assertEquals(2, count($data));
        $this->assertSame('test', $data[1]);

        // Sans append.

        $this->assertTrue($file->write('test', false));
        $data = $file->readAsArray();

        $this->assertIsArray($data);
        $this->assertEquals(1, count($data));
        $this->assertSame('test', $data[0]);

        $file->remove();
    }

    public function testGetDirectory(): void
    {
        $file = $this->instanciateTestFile();

        $this->assertInstanceOf(DirectoryInterface::class, $file->getDirectory());

        $file->remove();
    }

    public function testChmod(): void
    {
        $file = $this->instanciateTestFile();

        $this->assertTrue($file->chmod(0660));
        $this->assertSame(0660, $file->getPerms() & 0777);
        $this->assertSame('660', $file->getPerms(true));
        $this->assertSame(660, $file->permsStr);

        $file->remove();
    }

    public function testBasename(): void
    {
        $file = $this->instanciateTestFile();

        // Format du nom du fichier : "test_XXXXXXXXXXXXXXXX.txt".
        // Les X étant remplacés par des caractères alphanumériques,
        // on obtient 25 caractères en tout.

        $this->assertIsString($file->getBasename());
        $this->assertIsString($file->getBasename('.txt'));
        $this->assertIsString($file->getBasename('.doc'));

        $this->assertSame(25, strlen($file->getBasename()));
        $this->assertSame(21, strlen($file->getBasename('.txt')));
        $this->assertSame(25, strlen($file->getBasename('.doc')));

        $file->remove();
    }

    public function testProperties(): void
    {
        $file = $this->instanciateTestFile();

        $this->assertIsString($file->dirname);
        $this->assertIsString($file->basename);
        $this->assertIsString($file->extension);
        $this->assertIsString($file->filename);
        $this->assertIsString($file->path);
        $this->assertIsString($file->realPath);
        $this->assertIsInt($file->perms);
        $this->assertIsInt($file->permsStr);

        $file->remove();
    }

    public function testRemoveFile(): void
    {
        $this->assertTrue($this->instanciateTestFile()->remove());
    }

    public function testWriteReadFile(): void
    {
        $file = $this->instanciateTestFile();

        $this->assertTrue($file->write('test', false));

        $data = $file->readAsArray();

        $this->assertIsArray($data);
        $this->assertEquals(1, count($data));
        $this->assertSame('test', $data[0]);

        $file = $this->instanciateTestFile();

        $this->assertTrue($file->write(serialize(['hello' => 'world'])));

        $dataString = $file->read();
        $this->assertIsString($dataString);

        $dataArray = unserialize($dataString);
        $this->assertIsArray($dataArray);

        $this->assertArrayHasKey('hello', $dataArray);
        $this->assertSame('world', $dataArray['hello']);

        $file->remove();
    }
}
