<?php

namespace Ruiadr\Base\Tests\File;

use PHPUnit\Framework\TestCase;
use Ruiadr\Base\File\Directory;
use Ruiadr\Base\File\Exception\DirectoryAddException;
use Ruiadr\Base\File\Exception\DirectoryBadNameException;
use Ruiadr\Base\File\Exception\DirectoryNotFoundException;
use Ruiadr\Base\File\Exception\FileBadNameException;
use Ruiadr\Base\File\Interface\DirectoryInterface;
use Ruiadr\Base\File\Interface\FileBaseInterface;
use Ruiadr\Base\File\Interface\FileInterface;
use Ruiadr\Utils\SecurityUtils;

final class DirectoryTest extends TestCase
{
    private const TXT = '.txt';
    private const DOC = '.doc';
    private const TEST_ALL = 'test*';
    private const HELLO_ALL = 'hello*';

    private static ?DirectoryInterface $directory = null;

    public static function getDirectoryPathBase(): string
    {
        return dirname(__FILE__);
    }

    public static function generatedDirectoryPath(): string
    {
        return self::getDirectoryPathBase()
            .'/test_'.SecurityUtils::randomAlphanum();
    }

    public static function throwable(callable $func): ?\Exception
    {
        $exception = null;

        try {
            $func();
        } catch (\Throwable $e) {
            $exception = $e;
        }

        return $exception;
    }

    public function instanciateTestDirectory(): DirectoryInterface
    {
        if (self::$directory instanceof DirectoryInterface) {
            self::$directory->remove();
        }
        self::$directory = Directory::create(self::generatedDirectoryPath());

        return self::$directory;
    }

    public function testCreateDirectory(): void
    {
        $path = self::generatedDirectoryPath();

        $dir = null;

        try {
            $dir = Directory::create($path);
        } catch (\Exception $e) {
            // Ne rien faire pour permettre à la suite du script
            // de tester l'existence du répertoire.
        }

        $this->assertDirectoryExists($path);

        $this->assertSame("$dir", $path); // __toString().

        if ($dir instanceof DirectoryInterface) {
            $dir->remove();
        }
    }

    public function testCreateError(): void
    {
        $this->expectException(DirectoryAddException::class);
        Directory::create('/not_directory');
    }

    public function testBuildError(): void
    {
        $this->expectException(DirectoryNotFoundException::class);
        new Directory('/not_directory');
    }

    public function testCreateFileError(): void
    {
        $dir = Directory::create(self::generatedDirectoryPath());

        $fileBadNameException = false;

        try {
            $dir->createFile('my_directory/my_file');
        } catch (FileBadNameException $e) {
            $fileBadNameException = true;
        } catch (\Exception $e) {
            // Ne rien faire.
        }

        $this->assertTrue($fileBadNameException);

        if ($dir instanceof DirectoryInterface) {
            $dir->remove();
        }
    }

    public function testCreateDirectoryError(): void
    {
        $dir = Directory::create(self::generatedDirectoryPath());

        $directoryBadNameException = false;

        try {
            $dir->createDirectory('my_directory/my_file');
        } catch (DirectoryBadNameException $e) {
            $directoryBadNameException = true;
        } catch (\Exception $e) {
            // Ne rien faire.
        }

        $this->assertTrue($directoryBadNameException);

        if ($dir instanceof DirectoryInterface) {
            $dir->remove();
        }
    }

    public function testChmod(): void
    {
        $dir = $this->instanciateTestDirectory();

        $this->assertTrue($dir->chmod(0770));
        $this->assertSame(0770, $dir->getPerms() & 0777);
        $this->assertSame('770', $dir->getPerms(true));
        $this->assertSame(770, $dir->permsStr);

        $dir->remove();
    }

    public function testBasename(): void
    {
        $dir = $this->instanciateTestDirectory();

        // Format du nom du répertoire : "test_XXXXXXXXXXXXXXXX".
        // Les X étant remplacés par des caractères alphanumériques,
        // on obtient 21 caractères en tout.

        $this->assertIsString($dir->getBasename());
        $this->assertIsString($dir->getBasename('.txt'));
        $this->assertIsString($dir->getBasename('.doc'));

        $this->assertSame(21, strlen($dir->getBasename()));
        $this->assertSame(21, strlen($dir->getBasename('.txt')));
        $this->assertSame(21, strlen($dir->getBasename('.doc')));

        $dir->remove();
    }

    public function testProperties(): void
    {
        $dir = $this->instanciateTestDirectory();

        $this->assertIsString($dir->dirname);
        $this->assertIsString($dir->basename);
        $this->assertNull($dir->extension);
        $this->assertIsString($dir->filename);
        $this->assertIsString($dir->path);
        $this->assertIsString($dir->realPath);
        $this->assertIsInt($dir->perms);
        $this->assertIsInt($dir->permsStr);

        $dir->remove();
    }

    public function testCreateFileInner(): void
    {
        $dir = $this->instanciateTestDirectory();
        $file = $dir->createFile('test.txt');

        $this->assertFileExists($file->path);
        $this->assertTrue($file->remove());
        $this->assertFileDoesNotExist($file->path);

        $dir->remove();
    }

    public function testCreateDirectoryInner(): void
    {
        $parentDir = $this->instanciateTestDirectory();
        $dir = $parentDir->createDirectory('test');

        $this->assertDirectoryExists($dir->path);
        $this->assertTrue($dir->remove());
        $this->assertDirectoryDoesNotExist($dir->path);

        $parentDir->remove();
    }

    public function testRemoveDirectory(): void
    {
        $this->assertTrue($this->instanciateTestDirectory()->remove());
    }

    public function testList(): void
    {
        $dir = $this->instanciateTestDirectory();

        $this->assertIsArray($dir->list());
        $this->assertCount(0, $dir->list());

        $file1 = $dir->createFile('test1.txt');
        $file2 = $dir->createFile('test2.txt');
        $file3 = $dir->createFile('test3');

        $this->assertCount(3, $dir->list());

        $this->assertCount(2, $dir->list('*'.self::TXT));
        $this->assertCount(2, $dir->list('*'.self::TXT, FileBaseInterface::TYPE_FILE));
        $this->assertCount(0, $dir->list('*'.self::TXT, FileBaseInterface::TYPE_DIRECTORY));
        $this->assertCount(0, $dir->list('*'.self::DOC));

        $this->assertCount(2, $dir->list('/*'.self::TXT));
        $this->assertCount(2, $dir->list('/*'.self::TXT, FileBaseInterface::TYPE_FILE));
        $this->assertCount(0, $dir->list('/*'.self::TXT, FileBaseInterface::TYPE_DIRECTORY));
        $this->assertCount(0, $dir->list('/*'.self::DOC));

        $this->assertCount(2, $dir->listFiles('*'.self::TXT));
        $this->assertCount(0, $dir->listDirectories('*'.self::TXT));
        $this->assertCount(2, $dir->listFiles('/*'.self::TXT));
        $this->assertCount(0, $dir->listDirectories('/*'.self::TXT));

        $dir1 = $dir->createDirectory('test1');
        $dir2 = $dir->createDirectory('test2');

        $this->assertCount(5, $dir->list());

        $this->assertCount(5, $dir->list(self::TEST_ALL));
        $this->assertCount(2, $dir->list(self::TEST_ALL, FileBaseInterface::TYPE_DIRECTORY));
        $this->assertCount(3, $dir->list(self::TEST_ALL, FileBaseInterface::TYPE_FILE));
        $this->assertCount(0, $dir->list(self::HELLO_ALL));

        $this->assertCount(5, $dir->list('/'.self::TEST_ALL));
        $this->assertCount(2, $dir->list('/'.self::TEST_ALL, FileBaseInterface::TYPE_DIRECTORY));
        $this->assertCount(3, $dir->list('/'.self::TEST_ALL, FileBaseInterface::TYPE_FILE));
        $this->assertCount(0, $dir->list('/'.self::HELLO_ALL));

        $this->assertCount(3, $dir->listFiles(self::TEST_ALL));
        $this->assertCount(2, $dir->listDirectories(self::TEST_ALL));

        $this->assertCount(3, $dir->listFiles('/'.self::TEST_ALL));
        $this->assertCount(2, $dir->listDirectories('/'.self::TEST_ALL));

        foreach ($dir->listFiles() as $file) {
            $this->assertInstanceOf(FileInterface::class, $file);
        }

        foreach ($dir->listDirectories() as $directory) {
            $this->assertInstanceOf(DirectoryInterface::class, $directory);
        }

        foreach ($dir->list() as $entry) {
            $this->assertInstanceOf(FileBaseInterface::class, $entry);
        }

        $file1->remove();
        $file2->remove();
        $file3->remove();

        $dir1->remove();
        $dir2->remove();

        $dir->remove();
    }
}
