<?php

namespace Ruiadr\Base\Tests\System;

use PHPUnit\Framework\TestCase;
use Ruiadr\Base\System\Interface\OutputInterface;
use Ruiadr\Base\System\Output;

final class OutputTest extends TestCase
{
    private function buffer(callable $func): string
    {
        ob_start();
        $func();
        $out = ob_get_contents();
        ob_end_clean();

        return trim($out);
    }

    private function print(string $message, int $color, bool $newLine = false): string
    {
        return $this->buffer(function () use ($message, $color, $newLine) {
            if ($newLine) {
                Output::println($message, $color);
            } else {
                Output::print($message, $color);
            }
        });
    }

    private static function formattedOutput($message, $color): string
    {
        return "\033[{$color}m{$message}\033[0m";
    }

    public function testPrint(): void
    {
        $message = 'test';
        $color = OutputInterface::COLOR_DEFAULT;

        $out = $this->print($message, $color);
        $expected = $this->formattedOutput($message, $color);

        $this->assertSame($expected, $out);
    }

    public function testPrintNewLine(): void
    {
        $message = 'test';
        $color = OutputInterface::COLOR_DEFAULT;

        $out = $this->print($message, $color, true);
        $expected = $this->formattedOutput("$message\n", $color);

        $this->assertSame($expected, $out);
    }

    public function testPrintError(): void
    {
        $message = 'test';

        // Sans retour à la ligne.

        $out = $this->buffer(function () use ($message) {
            Output::printError($message);
        });
        $expected = $this->formattedOutput("$message", OutputInterface::COLOR_RED);

        $this->assertSame($expected, $out);

        // Avec retour à la ligne.

        $out = $this->buffer(function () use ($message) {
            Output::printError($message, true);
        });
        $expected = $this->formattedOutput("$message\n", OutputInterface::COLOR_RED);

        $this->assertSame($expected, $out);
    }

    public function testPrintSuccess(): void
    {
        $message = 'test';

        // Sans retour à la ligne.

        $out = $this->buffer(function () use ($message) {
            Output::printSuccess($message);
        });
        $expected = $this->formattedOutput("$message", OutputInterface::COLOR_GREEN);

        $this->assertSame($expected, $out);

        // Avec retour à la ligne.

        $out = $this->buffer(function () use ($message) {
            Output::printSuccess($message, true);
        });
        $expected = $this->formattedOutput("$message\n", OutputInterface::COLOR_GREEN);

        $this->assertSame($expected, $out);
    }

    public function testPrintWarning(): void
    {
        $message = 'test';

        // Sans retour à la ligne.

        $out = $this->buffer(function () use ($message) {
            Output::printWarning($message);
        });
        $expected = $this->formattedOutput("$message", OutputInterface::COLOR_YELLOW);

        $this->assertSame($expected, $out);

        // Avec retour à la ligne.

        $out = $this->buffer(function () use ($message) {
            Output::printWarning($message, true);
        });
        $expected = $this->formattedOutput("$message\n", OutputInterface::COLOR_YELLOW);

        $this->assertSame($expected, $out);
    }
}
