<?php

namespace Ruiadr\Base\Tests\Common;

use PHPUnit\Framework\TestCase;
use Ruiadr\Base\Common\Interface\TypeInterface;
use Ruiadr\Base\Common\Type;

final class TypeTest extends TestCase
{
    public function testCastString(): void
    {
        $this->assertSame('test', Type::cast('test', TypeInterface::STRING));
    }

    public function testCastInt(): void
    {
        $this->assertSame(1, Type::cast('1', TypeInterface::INT));
    }

    public function testCastFloat(): void
    {
        $this->assertSame(1.1, Type::cast('1.1', TypeInterface::FLOAT));
    }

    public function testCastStringError(): void
    {
        $this->assertNull(Type::cast('test', TypeInterface::INT));
    }

    public function testCastArrayError(): void
    {
        $this->assertNull(Type::cast(['test'], TypeInterface::STRING));
    }

    public function testAutoCast(): void
    {
        $this->assertSame(1, Type::autoCast('1'));
        $this->assertSame(1.1, Type::autoCast('1.1'));
        $this->assertSame(true, Type::autoCast(true));
        $this->assertSame(true, Type::autoCast('true'));
        $this->assertSame(false, Type::autoCast(false));
        $this->assertSame(false, Type::autoCast('false'));
        $this->assertSame([], Type::autoCast([]));
        $this->assertSame(null, Type::autoCast(null));
    }
}
