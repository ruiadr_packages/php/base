<?php

namespace Ruiadr\Base\Tests\Common;

use PHPUnit\Framework\TestCase;
use Ruiadr\Base\Common\Globals;

final class GlobalsTest extends TestCase
{
    public function testContains(): void
    {
        $GLOBALS['testContains'] = 1;

        $this->assertTrue(Globals::contains('testContains'));
    }

    public function testNotContains(): void
    {
        $this->assertFalse(Globals::contains('testNotContains'));
    }

    public function testGet(): void
    {
        $GLOBALS['testGet'] = 'testGet';

        $this->assertSame('testGet', Globals::get('testGet'));
    }

    public function testGetInt(): void
    {
        $GLOBALS['testGetInt'] = '1';
        $this->assertSame(1, Globals::getInt('testGetInt'));
        $this->assertSame(1, Globals::get('testGetInt'));

        $GLOBALS['testGetInt'] = 1;
        $this->assertSame(1, Globals::getInt('testGetInt'));
        $this->assertSame(1, Globals::get('testGetInt'));
    }

    public function testGetFloat(): void
    {
        $GLOBALS['testGetFloat'] = '1.1';
        $this->assertSame(1.1, Globals::getFloat('testGetFloat'));
        $this->assertSame(1.1, Globals::get('testGetFloat'));

        $GLOBALS['testGetFloat'] = 1.1;
        $this->assertSame(1.1, Globals::getFloat('testGetFloat'));
        $this->assertSame(1.1, Globals::get('testGetFloat'));
    }

    public function testGetString(): void
    {
        $GLOBALS['testGetString'] = '1.1';
        $this->assertSame('1.1', Globals::getString('testGetString'));
        $this->assertSame(1.1, Globals::get('testGetString'));

        $GLOBALS['testGetString'] = 1.1;
        $this->assertSame('1.1', Globals::getString('testGetString'));
        $this->assertSame(1.1, Globals::get('testGetString'));
    }

    public function testNotImmutable(): void
    {
        $o = Globals::build();

        $GLOBALS['a'] = 1;
        $v1 = $o->getParameters()->get('a');

        $GLOBALS['a'] = 2;
        $v2 = $o->getParameters()->get('a');

        $this->assertNotSame($v1, $v2);
    }
}
