<?php

namespace Ruiadr\Base\Tests\Common;

use PHPUnit\Framework\TestCase;
use Ruiadr\Base\Common\Interface\TypeInterface;
use Ruiadr\Base\Common\Parameters;

final class ParametersTest extends TestCase
{
    private const DEFAULT_DATA = [
        'a' => 1,
        'b' => 2,
        'c' => 'test',
        'd' => [],
        'e' => null,
    ];

    private function getParameters(?array $data = null): Parameters
    {
        if (!is_array($data)) {
            $data = self::DEFAULT_DATA;
        }

        return new Parameters($data);
    }

    public function testContains(): void
    {
        $parameters = $this->getParameters();

        $this->assertTrue($parameters->contains('a'));
        $this->assertTrue($parameters->contains('e'));
    }

    public function testNotContains(): void
    {
        $parameters = $this->getParameters();

        $this->assertFalse($parameters->contains('testNotContains'));
    }

    public function testGet(): void
    {
        $parameters = $this->getParameters();

        $this->assertSame('test', $parameters->get('c'));
        $this->assertInstanceOf(Parameters::class, $parameters->get('d'));
    }

    public function testGetDefaultValue(): void
    {
        $parameters = $this->getParameters([]);

        $this->assertSame('toto', $parameters->get('a', 'toto'));
        $this->assertSame('toto', $parameters->get('a', 'toto', TypeInterface::STRING));
        $this->assertNotSame('toto', $parameters->get('a', 'toto', TypeInterface::INT));
    }

    public function testNull(): void
    {
        $parameters = $this->getParameters([]);

        $this->assertNull($parameters->get('a'));
    }

    public function testProps(): void
    {
        $parameters = $this->getParameters();

        $this->assertSame(1, $parameters->a);
        $this->assertInstanceOf(Parameters::class, $parameters->d);
    }

    public function testPropsNotExists(): void
    {
        $parameters = $this->getParameters();

        $this->assertNull($parameters->toto);
    }

    public function testGetInt(): void
    {
        $parameters = $this->getParameters();

        $this->assertSame(2, $parameters->getInt('b'));
    }

    public function testGetString(): void
    {
        $parameters = $this->getParameters();

        $this->assertSame('test', $parameters->getString('c'));
    }

    public function testGetStringFromInt(): void
    {
        $parameters = $this->getParameters();

        $this->assertSame('2', $parameters->getString('b'));
    }

    public function testNotInt(): void
    {
        $parameters = $this->getParameters();

        $this->assertNull($parameters->getInt('c'));
    }

    public function testNotString(): void
    {
        $parameters = $this->getParameters();

        $this->assertNull($parameters->getString('d'));
    }

    public function testAutoCast(): void
    {
        // Cast auto à true par défaut:

        $parameters = $this->getParameters([
            'a' => '1',
            'b' => '1.1',
            'c' => 2,
            'd' => 2.2,
            'e' => true,
            'f' => [
                'a' => '1',
            ],
            'g' => 'true',
            'h' => null,
        ]);

        $this->assertIsInt($parameters->a);
        $this->assertIsFloat($parameters->b);
        $this->assertIsInt($parameters->c);
        $this->assertIsFloat($parameters->d);
        $this->assertTrue($parameters->e);
        $this->assertInstanceOf(Parameters::class, $parameters->f);
        $this->assertIsInt($parameters->f->a);
        $this->assertTrue($parameters->g);
        $this->assertNull($parameters->h);

        // On désactive le cast auto et on réalise les mêmes tests.

        $parameters->setAutoCast(false);

        $this->assertIsString($parameters->a);
        $this->assertIsString($parameters->b);
        $this->assertIsInt($parameters->c);
        $this->assertIsFloat($parameters->d);
        $this->assertTrue($parameters->e);
        $this->assertInstanceOf(Parameters::class, $parameters->f);
        $this->assertIsString($parameters->f->a);
        $this->assertIsString($parameters->g);
        $this->assertNull($parameters->h);
    }

    public function testEnv(): void
    {
        putenv('env_testEnv1=testEnv1');
        putenv('env_testEnv2=2');

        $parameters = $this->getParameters([
            'a' => 'ENV:env_testEnv1',
            'b' => 'ENV:env_testEnv2',
            'c' => 'ENV:env_testEnv3',
        ]);

        $this->assertSame('testEnv1', $parameters->a);
        $this->assertSame('testEnv1', $parameters->getString('a'));

        $this->assertSame(2, $parameters->b);
        $this->assertSame('2', $parameters->getString('b'));
        $this->assertSame(2, $parameters->getInt('b'));

        $this->assertNull($parameters->c);
        $this->assertNull($parameters->getString('c'));
    }

    public function testChain(): void
    {
        $data = [
            'a' => [
                'b' => 'hello world',
                'c' => [
                    'd' => 'hello',
                    'e' => 'world',
                ],
            ],
        ];

        $parameters = $this->getParameters($data);

        $this->assertSame('hello world', $parameters->a->b);
        $this->assertSame('hello', $parameters->a->c->d);
        $this->assertSame('world', $parameters->a->c->e);
        $this->assertNull($parameters->a->e);
    }

    public function testGetParams(): void
    {
        $parameters = $this->getParameters();

        $this->assertSame(self::DEFAULT_DATA, $parameters->getParams());
    }

    public function testIsset(): void
    {
        $parameters = $this->getParameters();

        $this->assertTrue(isset($parameters->a));
        $this->assertFalse(isset($parameters->x));
    }
}
