<?php

namespace Ruiadr\Base\Tests\Common;

use PHPUnit\Framework\TestCase;
use Ruiadr\Base\Common\Server;

final class ServerTest extends TestCase
{
    public function testGetScriptName(): void
    {
        $this->assertIsString(Server::getString('SCRIPT_NAME'));
    }
}
