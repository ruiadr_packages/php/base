<?php

namespace Ruiadr\Base\Tests\Http;

use PHPUnit\Framework\TestCase;
use Ruiadr\Base\Http\HttpPost;

final class HttpPostTest extends TestCase
{
    public function testContains(): void
    {
        $_POST['testContains'] = 1;

        $this->assertTrue(HttpPost::contains('testContains'));
    }

    public function testNotContains(): void
    {
        $this->assertFalse(HttpPost::contains('testNotContains'));
    }

    public function testGet(): void
    {
        $_POST['testGet'] = 'testGet';

        $this->assertSame('testGet', HttpPost::get('testGet'));
    }

    public function testGetInt(): void
    {
        $_POST['testGetInt'] = '1';
        $this->assertSame(1, HttpPost::getInt('testGetInt'));
        $this->assertSame(1, HttpPost::get('testGetInt'));

        $_POST['testGetInt'] = 1;
        $this->assertSame(1, HttpPost::getInt('testGetInt'));
        $this->assertSame(1, HttpPost::get('testGetInt'));
    }

    public function testGetFloat(): void
    {
        $_POST['testGetFloat'] = '1.1';
        $this->assertSame(1.1, HttpPost::getFloat('testGetFloat'));
        $this->assertSame(1.1, HttpPost::get('testGetFloat'));

        $_POST['testGetFloat'] = 1.1;
        $this->assertSame(1.1, HttpPost::getFloat('testGetFloat'));
        $this->assertSame(1.1, HttpPost::get('testGetFloat'));
    }

    public function testGetString(): void
    {
        $_POST['testGetString'] = '1.1';
        $this->assertSame('1.1', HttpPost::getString('testGetString'));
        $this->assertSame(1.1, HttpPost::get('testGetString'));

        $_POST['testGetString'] = 1.1;
        $this->assertSame('1.1', HttpPost::getString('testGetString'));
        $this->assertSame(1.1, HttpPost::get('testGetString'));
    }

    public function testNotImmutable(): void
    {
        $o = HttpPost::build();

        $_POST['a'] = 1;
        $v1 = $o->getParameters()->get('a');

        $_POST['a'] = 2;
        $v2 = $o->getParameters()->get('a');

        $this->assertNotSame($v1, $v2);
    }
}
