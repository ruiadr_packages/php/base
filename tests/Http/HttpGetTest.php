<?php

namespace Ruiadr\Base\Tests\Http;

use PHPUnit\Framework\TestCase;
use Ruiadr\Base\Http\HttpGet;

final class HttpGetTest extends TestCase
{
    public function testContains(): void
    {
        $_GET['testContains'] = 1;

        $this->assertTrue(HttpGet::contains('testContains'));
    }

    public function testNotContains(): void
    {
        $this->assertFalse(HttpGet::contains('testNotContains'));
    }

    public function testGet(): void
    {
        $_GET['testGet'] = 'testGet';

        $this->assertSame('testGet', HttpGet::get('testGet'));
    }

    public function testGetInt(): void
    {
        $_GET['testGetInt'] = '1';
        $this->assertSame(1, HttpGet::getInt('testGetInt'));
        $this->assertSame(1, HttpGet::get('testGetInt'));

        $_GET['testGetInt'] = 1;
        $this->assertSame(1, HttpGet::getInt('testGetInt'));
        $this->assertSame(1, HttpGet::get('testGetInt'));
    }

    public function testGetFloat(): void
    {
        $_GET['testGetFloat'] = '1.1';
        $this->assertSame(1.1, HttpGet::getFloat('testGetFloat'));
        $this->assertSame(1.1, HttpGet::get('testGetFloat'));

        $_GET['testGetFloat'] = 1.1;
        $this->assertSame(1.1, HttpGet::getFloat('testGetFloat'));
        $this->assertSame(1.1, HttpGet::get('testGetFloat'));
    }

    public function testGetString(): void
    {
        $_GET['testGetString'] = '1.1';
        $this->assertSame('1.1', HttpGet::getString('testGetString'));
        $this->assertSame(1.1, HttpGet::get('testGetString'));

        $_GET['testGetString'] = 1.1;
        $this->assertSame('1.1', HttpGet::getString('testGetString'));
        $this->assertSame(1.1, HttpGet::get('testGetString'));
    }

    public function testNotImmutable(): void
    {
        $o = HttpGet::build();

        $_GET['a'] = 1;
        $v1 = $o->getParameters()->get('a');

        $_GET['a'] = 2;
        $v2 = $o->getParameters()->get('a');

        $this->assertNotSame($v1, $v2);
    }
}
