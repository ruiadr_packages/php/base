<?php

namespace Ruiadr\Base\Common;

use Ruiadr\Base\Common\Interface\ParametersInterface;
use Ruiadr\Base\Common\Interface\TypeInterface;

class Parameters implements ParametersInterface
{
    /**
     * @param array $params          Tableau associatif contenant les données à exploiter
     * @param bool  $autoCastEnabled Activer ou non le cast auto, "true" par défaut
     */
    public function __construct(
        private readonly array $params,
        private bool $autoCastEnabled = true
    ) {
    }

    final public function getParams(): array
    {
        return $this->params;
    }

    final public function contains(string $name): bool
    {
        return array_key_exists($name, $this->params);
    }

    final public function setAutoCast(bool $enabled): ParametersInterface
    {
        $this->autoCastEnabled = $enabled;

        return $this;
    }

    /**
     * Retourne la valeur brute trouvée dans "$this->params" ou dans les
     * variables d'environnement lorsque cela est demandé.
     *
     * Par ex: si une valeur vaut <ParametersInterface::ENV_KEY>:<une_variable> alors
     * la méthode retournera la valeur de <une_variable> qui se trouve avec
     * les variables d'environnement, ou "null" dans le cas contraire.
     *
     * @param string $name Nom de clé
     *
     * @return mixed La valeur brute trouvée, "null" si rien n'a été trouvé
     */
    private function getRaw(string $name): mixed
    {
        if (!$this->contains($name)) {
            return null;
        }

        $result = $this->params[$name];

        if (is_string($result)) {
            // La valeur peut être une association "<ParametersInterface::ENV_KEY>:<une_variable>"
            // pour indiquer que la valeur réelle se trouve dans les variables d'environnement.
            $parts = explode(':', $result);
            if (2 === count($parts) && ParametersInterface::ENV_KEY === $parts[0]) {
                $envValue = getenv($parts[1]);

                return false !== $envValue ? $envValue : null;
            }
        }

        return $result;
    }

    final public function get(string $name, mixed $default = null, ?string $returnedType = null): mixed
    {
        $result = $this->getRaw($name);

        if (null === $result && null !== $default) {
            $result = $default;
        }

        if (null !== $result) {
            if (null !== $returnedType) {
                $result = Type::cast($result, $returnedType);
            } elseif ($this->autoCastEnabled) {
                $result = Type::autoCast($result);
            }
        }

        if (is_array($result)) {
            return new self($result, $this->autoCastEnabled);
        }

        return $result;
    }

    final public function getInt(string $name, ?int $default = null): ?int
    {
        return $this->get($name, $default, TypeInterface::INT);
    }

    final public function getFloat(string $name, ?float $default = null): ?float
    {
        return $this->get($name, $default, TypeInterface::FLOAT);
    }

    final public function getString(string $name, ?string $default = null): ?string
    {
        return $this->get($name, $default, TypeInterface::STRING);
    }

    final public function __get(string $name): mixed
    {
        return $this->get($name);
    }

    final public function __isset(string $name): bool
    {
        return $this->contains($name);
    }
}
