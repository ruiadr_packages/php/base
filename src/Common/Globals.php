<?php

namespace Ruiadr\Base\Common;

use Ruiadr\Base\Common\Base\ParametersBase;

class Globals extends ParametersBase
{
    final public function getData(): array
    {
        return $GLOBALS;
    }
}
