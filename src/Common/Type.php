<?php

namespace Ruiadr\Base\Common;

use Ruiadr\Base\Common\Interface\TypeInterface;

class Type implements TypeInterface
{
    final public static function cast(mixed $value, string $returnedType): mixed
    {
        if (!is_bool($value) && is_scalar($value) && TypeInterface::STRING === $returnedType) {
            return (string) $value;
        } elseif (is_numeric($value)) {
            return match ($returnedType) {
                TypeInterface::INT => (int) $value,
                TypeInterface::FLOAT => (float) $value,
                default => null,
            };
        }

        return null;
    }

    final public static function autoCast(mixed $value): mixed
    {
        if (is_numeric($value)) {
            return ctype_digit("$value") ? (int) $value : (float) $value;
        } elseif (is_string($value)) {
            return match ($value) {
                'true' => true,
                'false' => false,
                default => $value,
            };
        }

        return $value;
    }
}
