<?php

namespace Ruiadr\Base\Common\Base\Interface;

use Ruiadr\Base\Common\Interface\ParametersInterface;

interface ParametersBaseInterface
{
    /**
     * Retourne un objet de type "ParametersInterface" créé à partir du tableau
     * retourné par la méthode "getData()". Chaque appel à cette méthode
     * instancie un nouvel objet de type "ParametersInterface".
     *
     * @return ParametersInterface Objet de type "ParametersInterface"
     */
    public function getParameters(): ParametersInterface;

    /**
     * Retourne un tableau associatif contenant les données à exploiter par la classe.
     *
     * @return array Données à exploiter
     */
    public function getData(): array;

    /**
     * Construction de l'objet à partir des données retournées par la méthode "getData()".
     *
     * @return ParametersBaseInterface Objet héritant de "ParametersBaseInterface"
     */
    public static function build(): ParametersBaseInterface;

    /**
     * @see ParametersInterface::contains()
     */
    public static function contains(string $name): bool;

    /**
     * @see ParametersInterface::get()
     */
    public static function get(string $name, mixed $default = null, ?string $returnedType = null): mixed;

    /**
     * @see ParametersInterface::getInt()
     */
    public static function getInt(string $name, ?int $default = null): ?int;

    /**
     * @see ParametersInterface::getFloat()
     */
    public static function getFloat(string $name, ?float $default = null): ?float;

    /**
     * @see ParametersInterface::getString()
     */
    public static function getString(string $name, ?string $default = null): ?string;
}
