<?php

namespace Ruiadr\Base\Common\Base;

use Ruiadr\Base\Common\Base\Interface\ParametersBaseInterface;
use Ruiadr\Base\Common\Interface\ParametersInterface;
use Ruiadr\Base\Common\Parameters;

abstract class ParametersBase implements ParametersBaseInterface
{
    final public function getParameters(): ParametersInterface
    {
        return new Parameters($this->getData());
    }

    final public static function build(): ParametersBaseInterface
    {
        return new static();
    }

    private static function getParametersInstance(): ParametersInterface
    {
        return self::build()->getParameters();
    }

    final public static function contains(string $name): bool
    {
        return self::getParametersInstance()->contains($name);
    }

    final public static function get(
        string $name, mixed $default = null, ?string $returnedType = null
    ): mixed {
        return self::getParametersInstance()->get($name, $default, $returnedType);
    }

    final public static function getInt(string $name, ?int $default = null): ?int
    {
        return self::getParametersInstance()->getInt($name, $default);
    }

    final public static function getFloat(string $name, ?float $default = null): ?float
    {
        return self::getParametersInstance()->getFloat($name, $default);
    }

    final public static function getString(string $name, ?string $default = null): ?string
    {
        return self::getParametersInstance()->getString($name, $default);
    }
}
