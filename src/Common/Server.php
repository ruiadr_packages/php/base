<?php

namespace Ruiadr\Base\Common;

use Ruiadr\Base\Common\Base\ParametersBase;

class Server extends ParametersBase
{
    final public function getData(): array
    {
        return $_SERVER;
    }
}
