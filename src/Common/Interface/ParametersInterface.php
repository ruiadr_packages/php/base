<?php

namespace Ruiadr\Base\Common\Interface;

interface ParametersInterface
{
    final public const ENV_KEY = 'ENV';

    /**
     * Retourne une copie du tableau utilisé pour la construction de l'objet.
     *
     * @return array Tableau associé à l'objet
     */
    public function getParams(): array;

    /**
     * Retourne "true" si la clé "$name" existe.
     *
     * @param string $name Clé à vérifier
     *
     * @return bool "true" Si la clé "$name" existe bien, "false" sinon
     */
    public function contains(string $name): bool;

    /**
     * Pour activer/désactiver le cast auto effectué sur les valeurs retournées.
     * Le cast auto est actif par défaut, et pourrait ne pas convenir dans certains cas.
     *
     * @param bool $enabled "true"/"false" pour l'activer/désactiver
     *
     * @return ParametersInterface L'instance de l'objet
     */
    public function setAutoCast(bool $enabled): ParametersInterface;

    /**
     * Retourne la valeur du paramètre "$name", ou "$default" si la valeur
     * ne peut pas être retournée.
     *
     * Pour forcer un type de retour, il faut passer "$returnedType" avec l'une
     * des constantes suivantes:
     *      - TypeInterface::INT
     *      - TypeInterface::FLOAT
     *      - TypeInterface::STRING
     * Retournera "null" si le transtypage ne peut pas être réalisé, ou "$default" si une valeur
     * a été spécifiée.
     *
     * Par défaut, la méthode tente de caster le résultat dans le type approprié. Par exemple,
     * une donnée stockée comme '1' retournera l'entier correspondant, soit 1.
     *
     * Si la valeur d'une clé est de la forme "<ParametersInterface::ENV_KEY>:une_envvar",
     * alors la valeur sera récupérée depuis les variables d'environnement:
     *      - Variable d'environnement: HELLO=WORLD
     *      - ['test' => 'ENV:HELLO']
     *      - $this->get('test') retournera 'WORLD'
     *
     * @param string  $name         Clé pour laquelle récupérer une valeur
     * @param mixed   $default      Valeur par défaut si la clé n'est pas trouvée
     * @param ?string $returnedType Pour forcer le retour dans un type donné (cf TypeInterface::XXX)
     *
     * @return mixed Valeur de la clé "$name" lorsqu'elle existe, "null" sinon
     */
    public function get(string $name, mixed $default = null, ?string $returnedType = null): mixed;

    /**
     * Retourne la valeur du paramètre "$name" de type "int", ou "$default" si la
     * valeur ne peut pas être retournée.
     *
     * @param string $name    Clé pour laquelle récupérer une valeur
     * @param ?int   $default Valeur par défaut si la clé n'est pas trouvée
     *
     * @return ?int Valeur de la clé "$name" lorsqu'elle existe, "null" sinon
     */
    public function getInt(string $name, ?int $default = null): ?int;

    /**
     * Retourne la valeur du paramètre "$name" de type "float", ou "$default" si la
     * valeur ne peut pas être retournée.
     *
     * @param string $name    Clé pour laquelle récupérer une valeur
     * @param ?float $default Valeur par défaut si la clé n'est pas trouvée
     *
     * @return ?float Valeur de la clé "$name" lorsqu'elle existe, "null" sinon
     */
    public function getFloat(string $name, ?float $default = null): ?float;

    /**
     * Retourne la valeur du paramètre "$name" de type "string", ou "$default" si la
     * valeur ne peut pas être retournée.
     *
     * @param string  $name    Clé pour laquelle récupérer une valeur
     * @param ?string $default Valeur par défaut si la clé n'est pas trouvée
     *
     * @return ?string Valeur de la clé "$name" lorsqu'elle existe, "null" sinon
     */
    public function getString(string $name, ?string $default = null): ?string;

    /**
     * Méthode magique, retourne la valeur associée à la clé "$name"
     * lorsqu'elle existe, "null" sinon.
     *
     * La méthode assure le chaînage lorsqu'il y a plusieurs tableaux imbriqués, exemple:
     *      - $params = new Parameters(['a' => ['b' => 'hello world']]);
     *      - $params->a->b retourne 'hello world'
     *
     * @param string $name Clé pour laquelle récupérer une valeur
     *
     * @return mixed Valeur de la clé "$name" lorsqu'elle existe, "null" sinon
     */
    public function __get(string $name): mixed;

    /**
     * Déterminer si une clé existe ou non.
     *
     * @param string $name Clé utilisée pour identifier la valeur
     *
     * @return bool "true" si la clé existe, "false" sinon
     */
    public function __isset(string $name): bool;
}
