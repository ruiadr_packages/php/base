<?php

namespace Ruiadr\Base\Common\Interface;

interface TypeInterface
{
    final public const INT = 'int';
    final public const FLOAT = 'float';
    final public const STRING = 'string';

    /**
     * Assure que "$value" passée en paramètre soit bien du type "$returnedType"
     * avant de la retourner. Effectue un transtypage, ou retourne "null"
     * s'il n'y a pas de correspondance.
     *
     * @param mixed  $value        Valeur à transtyper
     * @param string $returnedType Type de retour souhaité (cf TypeInterface::XXX)
     *
     * @return mixed $value dans le type "$returnedType" lorsque c'est possible, "null" sinon
     */
    public static function cast(mixed $value, string $returnedType): mixed;

    /**
     * Applique un typage fort à "$value" passée en paramètre.
     * Si le typage fort ne peut être réalisé, "$value" est retournée telle quelle.
     *
     * @param mixed $value Valeur à transtyper automatiquement (cf TypeInterface::XXX)
     *
     * @return mixed $value Automatiquement transtypée lorsque c'est possible
     */
    public static function autoCast(mixed $value): mixed;
}
