<?php

namespace Ruiadr\Base\File\Exception;

class FileNotFoundException extends FileException
{
}
