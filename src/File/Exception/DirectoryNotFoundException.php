<?php

namespace Ruiadr\Base\File\Exception;

class DirectoryNotFoundException extends DirectoryException
{
}
