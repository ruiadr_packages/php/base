<?php

namespace Ruiadr\Base\File\Interface;

use Ruiadr\Base\File\Exception\DirectoryAddException;
use Ruiadr\Base\File\Exception\DirectoryNotFoundException;
use Ruiadr\Base\File\Exception\FileAddException;

interface FileInterface extends FileBaseInterface
{
    /**
     * Création d'une instance "File" pour le fichier "$path" passé en paramètre.
     * Si le fichier n'existe pas, la méthode va automatiquement le créer.
     * Si le fichier ne peut pas être créé, elle lève une exception
     * de type "FileAddException".
     * Le paramètre "$permissions" permet de spécifier le mode de permissions
     * à appliquer sur le fichier lors de sa création.
     *
     * @param string $path        Chemin du fichier
     * @param int    $permissions Mode de permissions à appliquer lors de la
     *                            création du fichier
     *
     * @return FileInterface Instance du fichier
     *
     * @throws FileAddException
     */
    public static function create(
        string $path, int $permissions = FileBaseInterface::DEFAULT_PERMISSIONS
    ): FileInterface;

    /**
     * Création du fichier "$path" dans son arborescence.
     * Si l'arborescence n'existe pas, elle est créée, et si elle ne peut
     * pas être créée, la méthode lève une exception de type "DirectoryAddException".
     * Idem pour le fichier, mais si le fichier ne peut pas être créé, la méthode
     * lève cette fois-ci une exception de type "FileAddException".
     * Le paramètre "$permissions" permet de spécifier le mode de permissions
     * à appliquer lors de la création du fichier. Le même mode de permissions
     * sera appliqué aux répertoires de l'arborescence.
     *
     * @param string $path        Chemin du fichier incluant l'arborescence
     * @param int    $permissions Mode de permissions à appliquer lors de la
     *                            création du fichier (sur le fichier et les répertoires)
     *
     * @return FileInterface Instance du fichier
     *
     * @throws DirectoryAddException
     * @throws FileAddException
     */
    public static function createTree(
        string $path, int $permissions = FileBaseInterface::DEFAULT_PERMISSIONS
    ): FileInterface;

    /**
     * Écrit le contenu "$content" dans le fichier courant.
     * Avec le paramètre "$append" à true, le contenu est ajouté en fin de fichier,
     * sinon le contenu du fichier est remplacé par "$content".
     * Retourne "false" si l'écriture a échoué, "true" sinon.
     *
     * @param string $content Contenu à écrire
     * @param bool   $append  "true" pour ajouter le contenu à la fin du fichier,
     *                        "false" pour remplacer l'ancien contenu
     */
    public function write(string $content, bool $append = true): bool;

    /**
     * Retourne le répertoire "DirectoryInterface" associé au fichier courant.
     * Lève une exception de type "DirectoryNotFoundException" si l'objet
     * "DirectoryInterface" ne peut être instancié.
     *
     * @return DirectoryInterface Instance "DirectoryInterface" du répertoire du fichier
     *
     * @throws DirectoryNotFoundException
     */
    public function getDirectory(): DirectoryInterface;

    /**
     * Lit le contenu du fichier courant et le retourne sous la forme d'un
     * tableau à raison d'un élément par ligne. La méthode utilise la
     * fonction PHP "file()": https://www.php.net/manual/en/function.file.php .
     * L'utilisation du paramètre "$flags" est documentée avec la fonction
     * PHP "file()".
     *
     * @param int $flags Se référer aux options de la fonction PHP "file()"
     *
     * @return array|false Contenu du fichier sous la forme de tableau, false en cas de problème
     */
    public function readAsArray(int $flags = 0): array|false;

    /**
     * Retourne le contenu du fichier courant. La méthode utilise la fonction PHP
     * "file_get_contents()": https://www.php.net/manual/en/function.file-get-contents.php .
     *
     * @return string|false Contenu du fichier, "false" en cas de problème
     */
    public function read(): string|false;
}
