<?php

namespace Ruiadr\Base\File\Interface;

use Ruiadr\Base\File\Exception\DirectoryAddException;
use Ruiadr\Base\File\Exception\DirectoryBadNameException;
use Ruiadr\Base\File\Exception\FileBadNameException;

interface DirectoryInterface extends FileBaseInterface
{
    /**
     * Création d'une instance "Directory" pour le répertoire "$path"
     * passé en paramètre. Si le répertoire n'existe pas, la méthode
     * va automatiquement le créer. Si le répertoire ne peut pas être créé,
     * elle lève une exception de type "DirectoryAddException".
     * Il est possible de créer une arborescence en la passant en paramètre,
     * par exemple "foo/bar".
     * Le paramètre "$permissions" permet de spécifier le mode de permissions
     * à appliquer sur le(s) répertoire(s) lors de sa création.
     *
     * @param string $path        Chemin du répertoire
     * @param int    $permissions Mode de permissions à appliquer lors de la création du répertoire
     *
     * @return DirectoryInterface Instance Directory
     *
     * @throws DirectoryAddException
     */
    public static function create(
        string $path, int $permissions = FileBaseInterface::DEFAULT_PERMISSIONS
    ): DirectoryInterface;

    /**
     * Liste sous la forme d'un tableau le contenu du répertoire courant en
     * instanciant des objets de types "DirectoryInterface".
     *
     * @param string $pattern Appliquer un filtre sur les résultats
     *
     * @return array Liste des répertoires (DirectoryInterface)
     */
    public function listDirectories(string $pattern = '*'): array;

    /**
     * Liste sous la forme d'un tableau le contenu du répertoire courant en
     * instanciant des objets de types "FileInterface".
     *
     * @param string $pattern Appliquer un filtre sur les résultats
     *
     * @return array Liste des fichiers (FileInterface)
     */
    public function listFiles(string $pattern = '*'): array;

    /**
     * Liste sous la forme d'un tableau, le contenu du répertoire courant en
     * instanciant automatiquement des objets de types "File" lorsqu'il s'agit
     * de fichiers, et "Directory" lorsqu'il s'agit de répertoires.
     *
     * Le paramètre "$pattern" permet de filtrer les éléments à lister, qu'ils
     * soient des répertoires ou des fichiers. Par exemple:
     *      - '*.txt'
     *      - '*.doc'
     *      - 'test*'
     *      - etc.
     *
     * Le paramètre "$type" permet d'assurer le type de retour:
     *      - FileBaseInterface::TYPE_FILE pour retourner uniquement des fichiers
     *      - FileBaseInterface::TYPE_DIRECTORY pour retourner uniquement des répertoires
     *
     * @param string $pattern Appliquer un filtre sur les résultats
     * @param int    $type    Retourner uniquement des fichiers ou des répertoires
     *
     * @return array Liste des fichiers (FileInterface) ou répertoires (DirectoryInterface)
     */
    public function list(string $pattern = '*', int $type = 0): array;

    /**
     * Création du fichier "$name" passé en paramètre dans le répertoire courant.
     * Le nom du fichier ne doit pas contenir d'arborescence ou la méthode
     * lèvera une exception de type "FileBadNameException".
     * La création du fichier est ensuite assurée par la classe "File", pour
     * plus d'informations, se référer à la méthode "File::create()".
     * Le paramètre "$permissions" permet de spécifier le mode de permissions
     * à appliquer sur le fichier lors de sa création.
     *
     * @param string $name        Nom du fichier
     * @param int    $permissions Mode de permissions à appliquer lors de la création du fichier
     *
     * @return FileInterface Instance File
     *
     * @throws FileBadNameException
     */
    public function createFile(
        string $name, int $permissions = FileBaseInterface::DEFAULT_PERMISSIONS
    ): FileInterface;

    /**
     * Création du répertoire "$name" passé en paramètre dans le répertoire courant.
     * Le nom du répertoire ne doit pas contenir d'arborescence ou la méthode
     * lèvera une exception de type "DirectoryBadNameException".
     * La création du répertoire est ensuite assurée par la classe "Directory", pour
     * plus d'informations, se référer à la méthode "Directory::create()".
     * Le paramètre "$permissions" permet de spécifier le mode de permissions
     * à appliquer sur le répertoire lors de sa création.
     *
     * @param string $name        Nom du répertoire
     * @param int    $permissions Mode de permissions à appliquer lors de la création du répertoire
     *
     * @return DirectoryInterface Instance Directory
     *
     * @throws DirectoryBadNameException
     */
    public function createDirectory(
        string $name, int $permissions = FileBaseInterface::DEFAULT_PERMISSIONS
    ): DirectoryInterface;
}
