<?php

namespace Ruiadr\Base\File\Interface;

use Ruiadr\Base\File\Exception\AddException;

/**
 * @property string $dirname
 * @property string $basename
 * @property string $extension
 * @property string $filename
 * @property string $path
 * @property string $realPath
 * @property string $perms
 * @property string $permsStr
 */
interface FileBaseInterface
{
    final public const DEFAULT_PERMISSIONS = 0777;
    final public const TYPE_FILE = 1;
    final public const TYPE_DIRECTORY = 2;

    /**
     * Création d'une instance pour l'élément "$path" passé en paramètre.
     * Si l'élément n'existe pas, la méthode va automatiquement le créer.
     * Si l'élément ne peut pas être créé, elle lève une exception
     * de type "AddException".
     * Le paramètre "$permissions" permet de spécifier le mode de permissions
     * à appliquer sur l'élément lors de sa création.
     *
     * @param string $path        Chemin de l'élément
     * @param int    $permissions Mode de permissions à appliquer lors de la
     *                            création de l'élément
     *
     * @return FileBaseInterface Instance de l'élément
     *
     * @throws AddException
     */
    public static function create(
        string $path, int $permissions = FileBaseInterface::DEFAULT_PERMISSIONS
    ): FileBaseInterface;

    /**
     * Retourne le chemin qui a servi à construire l'objet.
     *
     * @return string Chemin de l'élément
     */
    public function getPath(): string;

    /**
     * Retourne le chemin absolu de l'élément, "false" en cas de problème.
     *
     * @return string|false Chemin absolu de l'élément, "false" en cas de problème
     */
    public function getRealPath(): string|false;

    /**
     * Donne des informations sur l'élément. La méthode utilise
     * la fonction PHP "pathinfo()":
     * https://www.php.net/manual/fr/function.pathinfo.php .
     *
     * @return array Résultat de la fonction PHP "pathinfo()"
     */
    public function getPathinfo(): array;

    /**
     * Retourne le nom de la composante finale du chemin de l'objet.
     * La méthode utilise la fonction PHP "basename()":
     * https://www.php.net/manual/fr/function.basename.php .
     *
     * @param string $suffix S'il est fourni, le suffixe sera aussi supprimé
     *
     * @return string Retourne le nom de base du chemin qui a servi à construire l'objet
     */
    public function getBasename(string $suffix = ''): string;

    /**
     * Remplace le mode de l'élément par le mode de "$permissions" passé
     * en paramètre. Retourne "true" en cas de succès, "false" sinon.
     *
     * @param int $permissions Mode de permissions à appliquer sur l'élément
     *
     * @return bool true si le mode de permissions a été appliqué, "false" sinon
     */
    public function chmod(int $permissions): bool;

    /**
     * Suppression de l'élément. Retourne "true" s'il a bien été supprimé,
     * "false" sinon. Aucune vérification n'est réalisée, c'est simplement
     * le retour de la fonction PHP "unlink()" qui sert de résultat.
     *
     * @return bool "true" si l'élément a été supprimé, "false" sinon
     */
    public function remove(): bool;

    /**
     * Retourne les permissions de l'élément, ou "false" en cas d'erreur.
     * Si le paramètre "$asString" vaut "true", alors la méthode retournera
     * le résultat sous la forme d'une chaîne (ex: '777', '664', etc.),
     * sinon sous la forme d'un entier.
     *
     * Appelle la méthode "clearStatCache()" pour assurer la fraîcheur des
     * informations retournées.
     *
     * @param bool $asString "true" pour retourner une chaîne, "false" pour un entier
     *
     * @return int|string|false "int" si un entier est souhaité en retour,
     *                          ou "string", "false" en cas de problème
     */
    public function getPerms(bool $asString = false): int|string|false;

    /**
     * Supprime le cache de statut de l'élément permettant de récupérer des
     * informations fraîches.
     */
    public function clearStatCache(): void;

    /**
     * Méthode magique permettant d'accéder directement aux informations
     * systèmes de l'élément.
     *
     * Exemple d'utilisation:
     *      $o->dirname
     *      $o->filename
     *      $o->perms
     *      etc.
     *
     * Retourne "null" si la propriété ciblée n'existe pas.
     *
     * @param string $name Nom de la propriété à retourner
     *
     * @return mixed Valeur demandée, ou "null" si "$name" ne peut être retournée
     */
    public function __get(string $name): mixed;

    /**
     * Retourne le chemin qui a servi à construire l'objet
     * lorsqu'il est utilisé en tant que "string".
     *
     * @return string Chemin de l'élément ayant servi à la construction de l'objet
     */
    public function __toString(): string;
}
