<?php

namespace Ruiadr\Base\File;

use Ruiadr\Base\Common\Type;
use Ruiadr\Base\File\Interface\FileBaseInterface;
use Ruiadr\Utils\StringUtils;

abstract class FileBase implements FileBaseInterface
{
    /**
     * @param string $path Chemin de l'élément utilisé pour la création de l'objet
     */
    public function __construct(protected readonly string $path)
    {
    }

    final public function getPath(): string
    {
        return $this->path;
    }

    final public function getRealPath(): string|false
    {
        return realpath($this->path);
    }

    final public function getPathinfo(): array
    {
        return pathinfo($this->path);
    }

    final public function getBasename(string $suffix = ''): string
    {
        return basename($this->path, $suffix);
    }

    final public function chmod(int $permissions): bool
    {
        return @chmod($this->path, $permissions);
    }

    final public function getPerms(bool $asString = false): int|string|false
    {
        $this->clearStatCache();
        $result = @fileperms($this->path);

        return $asString ? decoct($result & 0777) : $result;
    }

    final public function clearStatCache(): void
    {
        clearstatcache(true, $this->path);
    }

    final public function __get(string $name): mixed
    {
        $pathInfo = $this->getPathinfo();
        if (array_key_exists($name, $pathInfo)) {
            return Type::autoCast($pathInfo[$name]);
        }

        $authProperties = [
            'path' => [],
            'realPath' => [],
            'perms' => [],
            'permsStr' => ['alias' => 'perms', 'args' => [true]],
        ];

        if (array_key_exists($name, $authProperties)) {
            $alias = $authProperties[$name]['alias'] ?? $name;
            $method = 'get'.StringUtils::capitalize($alias);

            if (method_exists($this, $method)) {
                $args = $authProperties[$name]['args'] ?? [];

                return Type::autoCast(call_user_func_array([$this, $method], $args));
            }
        }

        return null;
    }

    final public function __toString(): string
    {
        return $this->path;
    }
}
