<?php

namespace Ruiadr\Base\File;

use Ruiadr\Base\File\Exception\FileAddException;
use Ruiadr\Base\File\Exception\FileNotFoundException;
use Ruiadr\Base\File\Interface\DirectoryInterface;
use Ruiadr\Base\File\Interface\FileBaseInterface;
use Ruiadr\Base\File\Interface\FileInterface;

class File extends FileBase implements FileInterface
{
    private ?DirectoryInterface $directory = null;

    /**
     * @param string $path Chemin du fichier utilisé pour la création de l'objet. Si le
     *                     fichier n'existe pas, le constructeur lève une exception
     *                     de type "FileNotFoundException"
     *
     * @throws FileNotFoundException
     */
    public function __construct(string $path)
    {
        if (!@is_file($path)) {
            throw new FileNotFoundException("File not found: {$path}");
        }

        parent::__construct($path);
    }

    final public static function create(
        string $path, int $permissions = FileBaseInterface::DEFAULT_PERMISSIONS
    ): FileInterface {
        if (!@is_file($path) && !@touch($path)) {
            throw new FileAddException("Unable to create file: {$path}");
        }

        $file = new self($path);
        $file->chmod($permissions);

        return $file;
    }

    final public static function createTree(
        string $path, int $permissions = FileBaseInterface::DEFAULT_PERMISSIONS
    ): FileInterface {
        Directory::create(dirname($path), $permissions);

        return self::create($path, $permissions);
    }

    final public function write(string $content, bool $append = true): bool
    {
        return false !== file_put_contents($this->path, "$content", $append ? FILE_APPEND : 0);
    }

    final public function getDirectory(): DirectoryInterface
    {
        return $this->directory ??= new Directory(dirname($this->path));
    }

    final public function read(): string|false
    {
        return @file_get_contents($this->path);
    }

    final public function readAsArray(int $flags = 0): array|false
    {
        return @file($this->path, $flags);
    }

    final public function remove(): bool
    {
        return @unlink($this->path);
    }
}
