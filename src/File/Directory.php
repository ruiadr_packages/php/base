<?php

namespace Ruiadr\Base\File;

use Ruiadr\Base\File\Exception\DirectoryAddException;
use Ruiadr\Base\File\Exception\DirectoryBadNameException;
use Ruiadr\Base\File\Exception\DirectoryNotFoundException;
use Ruiadr\Base\File\Exception\FileBadNameException;
use Ruiadr\Base\File\Interface\DirectoryInterface;
use Ruiadr\Base\File\Interface\FileBaseInterface;
use Ruiadr\Base\File\Interface\FileInterface;

class Directory extends FileBase implements DirectoryInterface
{
    /**
     * @param string $path Chemin du répertoire utilisé pour la création de l'objet. Si le
     *                     répertoire n'existe pas, le constructeur lève une exception de type
     *                     "DirectoryNotFoundException". Il est possible de créer une
     *                     arborescence en la passant en paramètre, par exemple "foo/bar"
     *
     * @throws DirectoryNotFoundException
     */
    public function __construct(string $path)
    {
        if (!@is_dir($path)) {
            throw new DirectoryNotFoundException("Directory not found: {$path}");
        }

        parent::__construct($path);
    }

    final public static function create(
        string $path, int $permissions = FileBaseInterface::DEFAULT_PERMISSIONS
    ): DirectoryInterface {
        if (!@is_dir($path) && !@mkdir($path, $permissions, true)) {
            throw new DirectoryAddException("Unable to create directory: {$path}");
        }

        return new self($path);
    }

    /**
     * Récupérer la liste des paths des fichiers ou des répertoires contenus
     * dans le répertoire courant "$this->path".
     *
     * @param string $pattern Appliquer un filtre sur les résultats
     * @param int    $type    Retourner uniquement des fichiers ou des répertoires
     *                        (cf: FileBaseInterface::TYPE_XXX)
     *
     * @return array liste des paths
     */
    private function getPaths(string $pattern = '*', int $type = 0): array
    {
        $pattern = strlen($pattern) > 0 && '/' !== $pattern[0] ? '/'.$pattern : $pattern;

        return glob(
            $this->path.$pattern,
            FileBaseInterface::TYPE_DIRECTORY === $type ? GLOB_ONLYDIR : 0
        );
    }

    final public function listDirectories(string $pattern = '*'): array
    {
        $directories = [];

        $paths = $this->getPaths($pattern, FileBaseInterface::TYPE_DIRECTORY);
        foreach ($paths as $path) {
            $directories[] = new self($path);
        }

        return $directories;
    }

    final public function listFiles(string $pattern = '*'): array
    {
        $files = [];

        $paths = $this->getPaths($pattern, FileBaseInterface::TYPE_FILE);
        foreach (array_filter($paths, 'is_file') as $path) {
            $files[] = new File($path);
        }

        return $files;
    }

    final public function list(string $pattern = '*', int $type = 0): array
    {
        if ($type > 0) {
            return FileBaseInterface::TYPE_DIRECTORY === $type
                ? $this->listDirectories($pattern)
                : $this->listFiles($pattern);
        }

        $result = [];
        foreach ($this->getPaths($pattern) as $path) {
            $result[] = @is_file($path) ? new File($path) : new self($path);
        }

        return $result;
    }

    final public function remove(): bool
    {
        return @rmdir($this->path);
    }

    final public function createFile(
        string $name, int $permissions = FileBaseInterface::DEFAULT_PERMISSIONS
    ): FileInterface {
        if (str_contains($name, '/')) {
            throw new FileBadNameException("The file name must not contain any '/'");
        }

        return File::create($this->path.'/'.$name, $permissions);
    }

    final public function createDirectory(
        string $name, int $permissions = FileBaseInterface::DEFAULT_PERMISSIONS
    ): DirectoryInterface {
        if (str_contains($name, '/')) {
            throw new DirectoryBadNameException("The directory name must not contain any '/'");
        }

        return self::create($this->path.'/'.$name, $permissions);
    }
}
