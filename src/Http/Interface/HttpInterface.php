<?php

namespace Ruiadr\Base\Http\Interface;

interface HttpInterface
{
    final public const STATUS_SUCCESS_OK = 200;
    final public const STATUS_REDIRECT_PERMANENTLY = 301;
    final public const STATUS_REDIRECT_TEMPORARY = 302;
    final public const STATUS_ERROR_NOTFOUND = 404;
    final public const STATUS_SERVER_INTERNAL = 500;
}
