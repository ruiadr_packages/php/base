<?php

namespace Ruiadr\Base\Http;

use Ruiadr\Base\Common\Base\ParametersBase;
use Ruiadr\Base\Http\Interface\HttpInterface;

abstract class HttpBase extends ParametersBase implements HttpInterface
{
}
