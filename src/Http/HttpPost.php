<?php

namespace Ruiadr\Base\Http;

class HttpPost extends HttpBase
{
    final public function getData(): array
    {
        return $_POST;
    }
}
