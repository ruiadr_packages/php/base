<?php

namespace Ruiadr\Base\Http;

class HttpGet extends HttpBase
{
    final public function getData(): array
    {
        return $_GET;
    }
}
