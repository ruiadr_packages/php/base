<?php

namespace Ruiadr\Base\Wrapper;

use Ruiadr\Base\File\File;
use Ruiadr\Base\Wrapper\Interface\UrlInterface;
use Ruiadr\Utils\StringUtils;

final class Url implements UrlInterface
{
    /**
     * @param $url Url sur laquelle travailler
     */
    public function __construct(private readonly string $url)
    {
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function isValid(): bool
    {
        return false !== filter_var($this->url, FILTER_VALIDATE_URL);
    }

    public function getDomain(): ?string
    {
        if (!$this->isValid()) {
            return null;
        }

        $file = new File(dirname(__FILE__, 3).'/misc/tlds-alpha-by-domain.txt');
        $host = strtoupper($this->host);

        // Première étape :
        //   - media.monsite.com > com
        //   - media.monsite.co.uk > co.uk
        //   - media.monsite.google.com > com
        //   - media.monsite.google > google
        $suffix = $file->readAsArray(FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        $intersect = array_intersect(array_slice(explode('.', $host), -2), $suffix);

        // ATTENTION : ce script ne couvre pas la totalité des possibilités, il
        // peut convenir pour la plupart des cas.
        // Si nous prenons l'exemple de "google", en plus d'être un nom de domaine
        // (google.com, google.co.uk), il est aussi une extension (monsite.google).
        // Ici on se contentera d'un simple contrôle pour déterminer si le résultat
        // de l'intersection est à considérer comme une extension ou pas.

        // Extension en 2 parties ? (ex : co.uk)
        if (2 === count($intersect) && strlen($intersect[0]) > 3) {
            // On retire la première partie de l'extension jugée ici trop longue.
            array_shift($intersect);
        }

        $root = implode('.', $intersect);

        // Seconde étape :
        //   - media.monsite.com > monsite
        //   - media.monsite.co.uk > monsite
        //   - media.monsite.google.com > google
        //   - media.monsite.google > monsite
        $diff = explode('.', str_replace(".$root", '', $host));
        $name = array_pop($diff);

        return strtolower($name.'.'.$root);
    }

    public function __get(string $name): ?string
    {
        $result = null;

        // On commence par regarder du côté des constantes natives.
        try {
            $data = parse_url($this->url, constant('PHP_URL_'.strtoupper($name)));
        } catch (\Error $e) {
            $data = null;
        }

        // Si rien n'a été trouvé, on regarde s'il n'existe pas une méthode
        // de la classe à lier à la propriété utilisée.
        if (null === $data) {
            $method = 'get'.StringUtils::capitalize($name);
            if (method_exists($this, $method)) {
                $data = call_user_func([$this, $method]);
            }
        }

        if (is_string($data)) {
            $result = $data;
        }

        return $result;
    }

    public function __toString(): string
    {
        return $this->url;
    }
}
