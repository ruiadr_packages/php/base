<?php

namespace Ruiadr\Base\Wrapper;

use Ruiadr\Base\Common\Interface\TypeInterface;
use Ruiadr\Base\Common\Type;
use Ruiadr\Base\Wrapper\Interface\CUrlInterface;
use Ruiadr\Utils\StringUtils;

final class CUrl implements CUrlInterface
{
    private ?\CurlHandle $handle = null;
    private ?string $response = null;
    private array $infos = [];

    /**
     * @param $url     URL sur laquelle travailler
     * @param $options Liste des options cURL à utiliser. Lors de l'appel à la méthode
     *                 "exec()", les options seront utilisées via la fonction PHP "curl_setopt()".
     */
    public function __construct(
        private readonly string $url,
        private array $options = [])
    {
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function exec(): CUrlInterface
    {
        if (null === $this->response) {
            $this->handle = curl_init($this->url);

            $defaultOptions = [
                CURLOPT_TIMEOUT => CUrlInterface::DEFAULT_TIMEOUT,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_RETURNTRANSFER => true,
            ];

            curl_setopt_array($this->handle, $defaultOptions + $this->options);

            $response = curl_exec($this->handle);
            if (is_string($response)) {
                $this->response = $response;
                $this->infos = curl_getinfo($this->handle);
            }
        }

        return $this;
    }

    public function fetch(): string|false
    {
        $this->exec();

        return is_string($this->response) ? $this->response : false;
    }

    public function getResponse(): ?string
    {
        return $this->response;
    }

    public function getInfos(): array
    {
        return $this->infos;
    }

    /**
     * Surcharge les options "CURLOPT_HEADER" et "CURLOPT_NOBODY" de la prochaine
     * requête de telle sorte à optimiser le temps de réponse, en demandant
     * à recevoir uniquement les en-têtes.
     *
     * La surcharge est réalisée uniquement si "$this->response" ne contient rien,
     * autrement dit, si la méthode "exec()" n'a pas encore été appelée ou n'a
     * pas encore généré de réponse valide.
     *
     * @return ?array La valeur des options "CURLOPT_HEADER" et "CURLOPT_NOBODY"
     *                avant la surcharge, permettant de rétablir ultérieurement
     *                les valeurs éventuellement passées au constructeur. Retourne
     *                "null" si la surcharge intervient trop tard, c'est-à-dire que la méthode
     *                "exec()" a déjà été appelée, car dans ce cas on peut déjà
     *                exploiter le contenu dans l'en-tête HTTP.
     */
    private function addOnlyHeaderOptions(): ?array
    {
        if (null !== $this->response) {
            return null;
        }

        $overridenOptions = [];
        foreach ([CURLOPT_HEADER, CURLOPT_NOBODY] as $option) {
            if (array_key_exists($option, $this->options)) {
                $overridenOptions[$option] = $this->options[$option];
            }
            $this->options[$option] = true;
        }

        return $overridenOptions;
    }

    /**
     * Supprime ou rétablit les options "CURLOPT_HEADER" et "CURLOPT_NOBODY"
     * de "$this->options" si elles se trouvent dans "$overriddenOptions" passé
     * en paramètre.
     *
     * @param array $overriddenOptions Options surchargées pour la requête qui
     *                                 vient d'être exécutée
     */
    private function removeOnlyHeaderOptions(array $overriddenOptions): void
    {
        foreach ([CURLOPT_HEADER, CURLOPT_NOBODY] as $option) {
            if (array_key_exists($option, $overriddenOptions)) {
                $this->options[$option] = $overriddenOptions[$option];
            } else {
                unset($this->options[$option]);
            }
        }
    }

    public function getInfo(int|string $option, ?string $returnType = null): mixed
    {
        // En appelant la méthode "getInfos()" on pourrait souhaiter récupérer uniquement
        // les valeurs des en-têtes HTTP sans forcément récupérer le contenu de l'URL.
        // En ce sens, la méthode "addOnlyHeaderOptions()" le détermine automatiquement
        // et ajuste les options de la requête.
        $overriddenOptions = $this->addOnlyHeaderOptions();

        if (null === $this->handle) {
            // Si on dispose déjà d'un handle, inutile de relancer la méthode "exec()"
            // pour la suite des traitements à effectuer dans cette méthode.
            $this->exec();
        }

        $info = null;
        if (is_string($option) && array_key_exists($option, $this->infos)) {
            $info = $this->infos[$option];
        } elseif (is_int($option) && $this->handle instanceof \CurlHandle) {
            $info = curl_getinfo($this->handle, $option);
        }

        if (is_array($overriddenOptions)) {
            // Si on a exécuté une requête destinée à récupérer uniquement des
            // informations sur l'en-tête HTTP, alors on vide la réponse générée
            // pour permettre à l'utilisateur de la classe de lancer un nouvel
            // "exec()" avec cette fois-ci le contenu de la réponse.
            $this->removeOnlyHeaderOptions($overriddenOptions);
            // Pour coller au test effectué par la méthode "exec()".
            $this->response = null;
        }

        return null !== $info && is_string($returnType)
            ? Type::cast($info, $returnType)
            : $info;
    }

    public function getContentType(): ?string
    {
        $contentType = $this->getInfo('content_type', TypeInterface::STRING);
        if (is_string($contentType)) {
            $contentType = explode(';', $contentType)[0];
        }

        return $contentType;
    }

    public function getResponseCode(): ?int
    {
        return $this->getInfo('http_code', TypeInterface::INT);
    }

    public function isContentType(string $target): bool
    {
        $contentType = $this->getContentType();

        return is_string($contentType) && false !== strpos(
            StringUtils::lowerTrim($contentType),
            StringUtils::lowerTrim($target)
        );
    }
}
