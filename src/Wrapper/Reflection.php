<?php

namespace Ruiadr\Base\Wrapper;

use Ruiadr\Base\Wrapper\Interface\ReflectionInterface;
use Ruiadr\Utils\StringUtils;

final class Reflection implements ReflectionInterface
{
    public static function build(
        string $name, ?string $prefix = null, ?string $suffix = null, ?string $namespace = null
    ): ?\ReflectionClass {
        $className = StringUtils::capitalize($name);

        if (is_string($prefix)) {
            $prefix = StringUtils::capitalize($prefix);
            $className = $prefix.$className;
        }

        if (is_string($suffix)) {
            $suffix = StringUtils::capitalize($suffix);
            $className = $className.$suffix;
        }

        $reflectionClass = null;

        // Sans le namespace.
        if (class_exists($className)) {
            $reflectionClass = (new \ReflectionClass($className));
        }
        // Avec le namespace.
        else {
            if (null === $namespace) {
                $namespace = ReflectionInterface::DEFAULT_BASE_NS; // Ruiadr.
            }

            if (class_exists($namespace.$className)) {
                $reflectionClass = (new \ReflectionClass($namespace.$className));
            } else {
                if (is_string($suffix)) {
                    $namespace = $namespace.$suffix.'\\';
                }

                if (class_exists($namespace.$className)) {
                    $reflectionClass = (new \ReflectionClass($namespace.$className));
                }
            }
        }

        return $reflectionClass;
    }

    public static function buildPrefix(
        string $name, string $prefix, ?string $namespace = null
    ): ?\ReflectionClass {
        return self::build($name, $prefix, null, $namespace);
    }

    public static function buildSuffix(
        string $name, string $suffix, ?string $namespace = null
    ): ?\ReflectionClass {
        return self::build($name, null, $suffix, $namespace);
    }
}
