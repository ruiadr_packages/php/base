<?php

namespace Ruiadr\Base\Wrapper\Interface;

/**
 * @property ?string $scheme   natif
 * @property ?string $host     natif
 * @property ?string $port     natif
 * @property ?string $user     natif
 * @property ?string $pass     natif
 * @property ?string $path     natif
 * @property ?string $query    natif
 * @property ?string $fragment natif
 * @property ?string $domain   propriété pour utiliser la méthode getDomain()
 */
interface UrlInterface
{
    /**
     * Retourne une copie de l'URL qui a servi à la construction de l'objet.
     *
     * @return string URL qui a servi à la construction de l'objet
     */
    public function getUrl(): string;

    /**
     * Retourne "true" si l'URL utilisée pour la construction de l'objet
     * est une URL valide, "false" sinon.
     *
     * @return bool "true" si l'URL a un format valide
     */
    public function isValid(): bool;

    /**
     * Retourne le domaine de l'URL utilisée pour la construction de l'objet.
     * La méthode s'appuie sur la définition des TLDs disponibles à cette adresse :
     * https://data.iana.org/TLD/tlds-alpha-by-domain.txt pour extraire un domaine
     * valide avec l'extension appropriée.
     *
     * Par exemple :
     *      - www.toto.com : toto.com
     *      - www.toto.co.uk : toto.co.uk
     *      - www.google.com : google.com
     *      - www.monsite.google : monsite.google
     *      - etc.
     *
     * Attention : cette méthode ne couvre pas la totalité des possibilités,
     * mais peut convenir dans la plupart des cas. Le résultat pourrait
     * donc ne pas convenir. À utiliser en connaissance de cause.
     *
     * @return string Le domaine de l'URL qui a servi à la construction de l'objet,
     *                ou "null" en cas d'erreur
     */
    public function getDomain(): ?string;

    /**
     * Permet d'accéder aux informations de l'URL de l'objet courant.
     * Par exemple : scheme, host, path, etc.
     * La classe apporte un certain nombre de fonctionnalités supplémentaires
     * qui sont accessibles grâce aux propriétés, par exemple : "domain".
     *
     * @param string $name Nom de la propriété à récupérer
     *
     * @return ?string La valeur si elle existe, "null" sinon
     */
    public function __get(string $name): ?string;

    /**
     * Retourne l'URL qui a servi à construire l'objet courant
     * lorsqu'il est utilisé en tant que "string".
     *
     * @return string URL ayant servi à la construction de l'objet
     */
    public function __toString(): string;
}
