<?php

namespace Ruiadr\Base\Wrapper\Interface;

interface CUrlInterface
{
    final public const DEFAULT_TIMEOUT = 10;

    /**
     * Exécute la requête cURL à l'aide des informations passées en paramètres
     * lors de la construction de l'objet.
     *
     * Options par défaut :
     *      - CURLOPT_TIMEOUT : DEFAULT_TIMEOUT
     *      - CURLOPT_FOLLOWLOCATION : true
     *      - CURLOPT_RETURNTRANSFER : true
     *
     * Toutes les options sont personnalisables depuis le constructeur
     * de la classe.
     *
     * @return CUrlInterface L'instance CUrl après avoir exécuté la requête cURL
     */
    public function exec(): CUrlInterface;

    /**
     * Retourne la réponse de la requête cURL si elle a donné un résultat, "false" sinon.
     *
     * @deprecated utiliser "exec()" et "getResponse()" à la place de la méthode "fetch()"
     *
     * @return string|bool Réponse ou "false" en cas de problème
     */
    public function fetch(): string|false;

    /**
     * Retourne la réponse sous la forme d'une chaîne, uniquement si
     * la requête a généré un résultat, "null" sinon.
     *
     * @return string La réponse, ou "null" si aucun résultat n'a pu être récupéré
     */
    public function getResponse(): ?string;

    /**
     * Retourne des informations sur la requête cURL lorsqu'elle a généré
     * un résultat. Le tableau est vide dans le cas contraire.
     * Cette méthode utilise la fonction PHP "curl_getinfo()" :
     * https://www.php.net/manual/fr/function.curl-getinfo.php .
     *
     * Ne lance pas la requête automatiquement, il faut au préalable
     * l'exécuter avec la méthode "exec()".
     *
     * @return array Résultat de la fonction PHP "curl_getinfo()"
     */
    public function getInfos(): array;

    /**
     * Retourne une information sur la requête cURL lorsqu'elle a généré
     * un résultat. Le paramètre "$option" permet d'identifier l'information à
     * récupérer.
     * "$option" peut être soit un entier c'est-à-dire une constante PHP, soit une chaîne,
     * cf la documentation de la fonction PHP "curl_getinfo()" :
     * https://www.php.net/manual/fr/function.curl-getinfo.php .
     * Le paramètre "$returnType" permet de spécifier un type de retour attendu.
     * En cas de problème, la méthode retournera "null".
     *
     * Optimisation : Lorsque cette méthode est appelée avant la méthode "exec()",
     * elle lance automatiquement la requête, permettant de récupérer "uniquement"
     * des informations sur l'en-tête HTTP, c'est-à-dire sans le contenu de la réponse
     * pour des questions de performance.
     *
     * Ex : si on souhaite récupérer uniquement les en-têtes d'une image, il ne faut
     * pas récupérer son contenu pour ne pas subir un temps d'attente relativement long.
     *
     * @param int|string $option     Clé de l'option à récupérer
     * @param ?string    $returnType Retour dans le type spécifié, null sinon (cf TypeInterface::XXX)
     *
     * @return mixed La valeur de l'option à récupérer
     */
    public function getInfo(int|string $option, ?string $returnType = null): mixed;

    /**
     * Retourne le "Content-Type" de la ressource appelée, "null" en cas d'erreur.
     *
     * @see getInfo() pour son mode de fonctionnement
     *
     * @return string Le "Content-Type" de la ressource ciblée, "null" en cas d'erreur
     */
    public function getContentType(): ?string;

    /**
     * Retourne le Code HTTP de la réponse, "null" en cas d'erreur.
     *
     * @see getInfo() pour son mode de fonctionnement
     *
     * @return string Le Code HTTP de la ressource ciblée, "null" en cas d'erreur
     */
    public function getResponseCode(): ?int;

    /**
     * Retourne "true" si le "Content-Type" de la ressource appelée est
     * identique à "$contentType" passé en paramètre. Retourne "false" dans le cas
     * contraire ou en cas d'erreur.
     *
     * @see getInfo() pour son mode de fonctionnement
     *
     * @param string $contentType Le "Content-Type" à tester
     *
     * @return bool "true" si la ressource ciblée est bien du "Content-Type" $contentType,
     *              "false" sinon
     */
    public function isContentType(string $contentType): bool;
}
