<?php

namespace Ruiadr\Base\Wrapper\Interface;

interface ReflectionInterface
{
    final public const DEFAULT_BASE_NS = '\Ruiadr\\';

    /**
     * Création d'une "\ReflectionClass".
     * Cette méthode peut être utilisée pour instancier des objets depuis
     * des factories, par exemple lorsque les noms des classes à construire
     * contiennent des parties reproductibles.
     *
     * Exemple :
     *      - Classe 1 : MyHtmlFetcher
     *      - Classe 2 : MyJavascriptFetcher
     *      - Classe 3 : MyStylesheetsFetcher
     *
     * On pourrait alors utiliser ceci :
     *      $type = 'html'; // ou javascript, ou stylesheets
     *      $reflectionClass = Reflection::build($type, 'my', 'fetcher');
     *      $object = $reflectionClass->newInstance($arg1, $arg2, etc.);
     *
     * Si aucune classe n'a été trouvée, alors la méthode retourne "null".
     *
     * @param string $name      Partie commune du nom de classe
     * @param string $prefix    Préfixe du nom de classe
     * @param string $suffix    Suffixe du nom de classe
     * @param string $namespace Namespace de la classe
     *
     * @return \ReflectionClass objet "\ReflectionClass" ou "null"
     */
    public static function build(
        string $name, ?string $prefix = null, ?string $suffix = null, ?string $namespace = null
    ): ?\ReflectionClass;

    /**
     * Cette méthode s'appuie sur la méthode "build", elle s'utilise
     * uniquement dans le cas où la partie reproductible du nom de classe
     * se trouve en préfixe.
     *
     * @param string $name      Partie commune du nom de classe
     * @param string $prefix    Préfixe du nom de classe
     * @param string $namespace Namespace de la classe
     *
     * @return \ReflectionClass Objet \ReflectionClass ou null
     */
    public static function buildPrefix(
        string $name, string $prefix, ?string $namespace = null
    ): ?\ReflectionClass;

    /**
     * Cette méthode s'appuie sur la méthode "build", elle s'utilise
     * uniquement dans le cas où la partie reproductible du nom de classe
     * se trouve en suffixe.
     *
     * @param string $name      Partie commune du nom de classe
     * @param string $suffix    Suffixe du nom de classe
     * @param string $namespace Namespace de la classe
     *
     * @return \ReflectionClass Objet \ReflectionClass ou null
     */
    public static function buildSuffix(
        string $name, string $suffix, ?string $namespace = null
    ): ?\ReflectionClass;
}
