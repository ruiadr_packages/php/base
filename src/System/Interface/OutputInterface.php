<?php

namespace Ruiadr\Base\System\Interface;

interface OutputInterface
{
    final public const COLOR_RED = 31;
    final public const COLOR_GREEN = 32;
    final public const COLOR_YELLOW = 33;
    final public const COLOR_DEFAULT = 39;

    /**
     * Affiche "$message" passé en paramètre sans retour à la ligne.
     *
     * @param string $message Message à afficher
     * @param int    $color   Couleur du message (cf. OutputInterface::COLOR_XXX)
     */
    public static function print(string $message, int $color): void;

    /**
     * Affiche "$message" passé en paramètre avec un retour à la ligne.
     *
     * @param string $message Message à afficher
     * @param int    $color   Couleur du message (cf. OutputInterface::COLOR_XXX)
     */
    public static function println(string $message, int $color): void;

    /**
     * Afficher un message de couleur rouge.
     *
     * @param string $message Message à afficher
     * @param bool   $newLine Afficher avec un retour à la ligne
     */
    public static function printError(string $message, bool $newLine = false): void;

    /**
     * Afficher un message de couleur verte.
     *
     * @param string $message Message à afficher
     * @param bool   $newLine Afficher avec un retour à la ligne
     */
    public static function printSuccess(string $message, bool $newLine = false): void;

    /**
     * Afficher un message de couleur jaune.
     *
     * @param string $message Message à afficher
     * @param bool   $newLine Afficher avec un retour à la ligne
     */
    public static function printWarning(string $message, bool $newLine = false): void;
}
