<?php

namespace Ruiadr\Base\System;

use Ruiadr\Base\System\Interface\OutputInterface;

final class Output implements OutputInterface
{
    final public static function print(
        string $message, int $color = OutputInterface::COLOR_DEFAULT
    ): void {
        echo "\033[{$color}m{$message}\033[0m";
    }

    final public static function println(
        string $message, int $color = OutputInterface::COLOR_DEFAULT
    ): void {
        self::print($message."\n", $color);
    }

    /**
     * Affiche "$message" passé en paramètre, de couleur "$color" avec ou sans retour à la ligne.
     *
     * @param string $message Message à afficher
     * @param int    $color   Couleur du message (cf. OutputInterface::COLOR_XXX)
     * @param bool   $newLine Afficher avec un retour à la ligne ou non
     */
    private static function printMessage(string $message, int $color, bool $newLine): void
    {
        $newLine ? self::println($message, $color) : self::print($message, $color);
    }

    final public static function printError(string $message, bool $newLine = false): void
    {
        self::printMessage($message, OutputInterface::COLOR_RED, $newLine);
    }

    final public static function printSuccess(string $message, bool $newLine = false): void
    {
        self::printMessage($message, OutputInterface::COLOR_GREEN, $newLine);
    }

    final public static function printWarning(string $message, bool $newLine = false): void
    {
        self::printMessage($message, OutputInterface::COLOR_YELLOW, $newLine);
    }
}
